#! /usr/bin/env bash

set -e

helm repo update

helm search repo ambassador/edge-stack
helm search repo argo/argo-cd
helm search repo argo/argocd-notifications
helm search repo argo/argo-rollouts
helm search repo jetstack/cert-manager
helm search repo newrelic/nri-bundle
