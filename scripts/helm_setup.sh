#! /usr/bin/env bash

set -e

# ambassador
helm repo add ambassador https://www.getambassador.io
# all argo projects (cd, notifications, workflows, etc
helm repo add argo https://argoproj.github.io/argo-helm
# cert-manager
helm repo add jetstack https://charts.jetstack.io
# New Relic
helm repo add newrelic https://helm-charts.newrelic.com
