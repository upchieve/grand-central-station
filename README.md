# Grand Central Station

Infrastructure code for UPchieve

This is our [Pulumi](https://pulumi.com) code for managing all our infrastructure.

The repo is split into three Pulumi projects:
* grand-central-station (gcs)
* terminal
* subway

## gcs

gcs contains the project/cloud level infrastructure. Azure resources, Aiven projects, etc.

Anything that is not specific to a given app or is created using Azure should go in gcs.

## terminal

terminal is our main kubernetes cluster. This directory holds code creating that cluster, and the tooling inside like Ambassador/ArgoCD/etc, as well as namespaces that represent environments, like demo/staging/production.

## subway

This dir holds infra specific to the [subway](https://gitlab.com/upchieve/subway) app. Redis instances, DNS, Argo app definitions, etc.

## ansible instructions 

Go to `marathon/ansible`.

# Getting set up

1. [Install Pulumi](https://www.pulumi.com/docs/get-started/install/)
1. Run `$ pulumi login` using the credentials in 1Password.
1. Run `$ npm install` in
  1. the top level directory
  2. gcs
  3. terminal
  4. subway
1. cd into the appropriate directory you're working in and run `$ pulumi up`

# Monitoring Kubernetes
There are a few tools out there to help you access our kubernetes cluster from your machine, such as:
- [kubectl](https://kubernetes.io/docs/reference/kubectl/)
- [k9s](https://k9scli.io/) (in-terminal GUI built on top of kubectl)

Either one requires you to have a properly set up [kubeconfig](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/) before you can connect.

To get help setting up your kubeconfig:
- Login to Azure, where our kubernetes cluster is hosted
- Under "Kubernetes Services" find the cluster, terminal
- In the "Overview" page, click the "Connect" tab. A side drawer will open with the instructions you'll need to connect to the cluster.
- Now you can start using kubectl and k9s.