import * as azure from '@pulumi/azure-native';
import { Resource, getResourceName, ConfigWrapper } from './utils';

export function createHighLine(
    resourceGroup: azure.resources.ResourceGroup,
) {
  return createStaticWebApp(resourceGroup)
}

function createStaticWebApp(
  resourceGroup: azure.resources.ResourceGroup,
) {
  const staticWebAppName = getResourceName(Resource.STATIC_WEB_APP)
  return new azure.web.StaticSite(staticWebAppName, {
    allowConfigFileUpdates: true,
    enterpriseGradeCdnStatus: ConfigWrapper.staticWebAppCdn,
    name: staticWebAppName,
    resourceGroupName: resourceGroup.name,
    sku: {
      tier: ConfigWrapper.staticWebAppSku,
      name: ConfigWrapper.staticWebAppSku,
    },
    stagingEnvironmentPolicy: 'Enabled',
  }, {
    protect: true,
  });
}
