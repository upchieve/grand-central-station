import * as azure from '@pulumi/azure-native'
import * as pulumi from '@pulumi/pulumi'
import { getResourceName, Resource, ConfigWrapper } from './utils'

export function createSubway(
  resourceGroup: azure.resources.ResourceGroup,
  virtualNetwork: azure.network.VirtualNetwork,
) {
  const registry = createContainerRegistry(resourceGroup)
  const registryManagedIdentity = createRegistryManagedIdentity(resourceGroup, registry)
  const subnet = createManagedEnvironmentSubnet(resourceGroup, virtualNetwork)
  const managedEnvironment = createManagedEnvironment(resourceGroup, subnet)
  const subwayContainerApp = createSubwayContainerApp(resourceGroup, managedEnvironment, registry, registryManagedIdentity)
  createWorkerContainerApp(resourceGroup, managedEnvironment, registry, registryManagedIdentity)
  return { managedEnvironment, subwayContainerApp }
}

function createContainerRegistry(resourceGroup: azure.resources.ResourceGroup) {
  const registryName = getResourceName(Resource.REGISTRY)
  return new azure.containerregistry.Registry(registryName, {
    registryName,
    resourceGroupName: resourceGroup.name,
    sku: {
      name: ConfigWrapper.containerRegistrySku,
    },
  });
}

function createRegistryManagedIdentity(
  resourceGroup: azure.resources.ResourceGroup,
  registry: azure.containerregistry.Registry
) {
  const managedIdentityName = getResourceName(Resource.REGISTRY_MANAGED_IDENTITY)
  const registryManagedIdentity = new azure.managedidentity.UserAssignedIdentity(managedIdentityName, {
    resourceGroupName: resourceGroup.name,
    resourceName: managedIdentityName,
  }, {
    protect: true,
  });

  const roleAssignmentName = getResourceName(Resource.REGISTRY_ROLE_ASSIGNMENT)
  new azure.authorization.RoleAssignment(roleAssignmentName, {
    principalId: registryManagedIdentity.principalId,
    principalType: 'ServicePrincipal',
    roleDefinitionId: ConfigWrapper.acrPullRoleDefinition.id,
    scope: registry.id,
  }, {
    protect: true,
  });

  return registryManagedIdentity
}

function createManagedEnvironmentSubnet(
  resourceGroup: azure.resources.ResourceGroup,
  virtualNetwork: azure.network.VirtualNetwork,
) {
  const subnetName = getResourceName(Resource.MANAGED_ENVIRONMENT_SUBNET)
  return new azure.network.Subnet(subnetName, {
    addressPrefix: '10.0.2.0/23',
    resourceGroupName: resourceGroup.name,
    subnetName,
    virtualNetworkName: virtualNetwork.name
  })
}

function createManagedEnvironment(
  resourceGroup: azure.resources.ResourceGroup,
  subnet: azure.network.Subnet
) {
  const managedEnvironmentName = getResourceName(Resource.MANAGED_ENVIRONMENT)
  return new azure.app.ManagedEnvironment(managedEnvironmentName, {
    appLogsConfiguration: {
      destination: ConfigWrapper.managedEnvironmentAppLogsDestination,
    },
    environmentName: managedEnvironmentName,
    resourceGroupName: resourceGroup.name,
    vnetConfiguration: {
      infrastructureSubnetId: subnet.id,
      internal: true,
    },
    zoneRedundant: ConfigWrapper.managedEnvironmentZoneRedundant,
  }, {
    protect: true,
  });
}

function createSubwayContainerApp(
  resourceGroup: azure.resources.ResourceGroup,
  managedEnvironment: azure.app.ManagedEnvironment,
  registry: azure.containerregistry.Registry,
  registryManagedIdentity: azure.managedidentity.UserAssignedIdentity,
) {
  const containerAppSubwayName = getResourceName(Resource.CONTAINER_APP_SUBWAY)
  return new azure.app.ContainerApp(containerAppSubwayName, {
    configuration: {
      activeRevisionsMode: 'Single',
      ingress: {
        allowInsecure: false,
        corsPolicy: {
          allowCredentials: true,
          allowedOrigins: [ConfigWrapper.appURL],
        },
        external: true,
        // TODO: Uncomment once can do this.
        // Otherwise, for now, set in the portal.
        // stickySessions: {
        //   affinity: 'sticky',
        // },
        targetPort: 3000,
        traffic: [{
          latestRevision: true,
          weight: 100,
        }],
        transport: 'Auto',
      },
      registries: [{
        identity: registryManagedIdentity.id,
        server: registry.loginServer,
      }],
      secrets: [
        {
          name: 'doppler-token',
          value: ' ', // Update this value in the portal once the resource is created.
        },
      ],
    },
    containerAppName: containerAppSubwayName,
    identity: {
      type: 'UserAssigned',
      userAssignedIdentities: [registryManagedIdentity.id],
    },
    environmentId: managedEnvironment.id,
    resourceGroupName: resourceGroup.name,
    template: {
      containers: [{
        image: pulumi.interpolate`${registry.loginServer}/subway:initial`,
        env: [{
          name: 'DOPPLER_TOKEN',
          secretRef: 'doppler-token',
        }],
        name: 'subway',
        probes: [
          {
            type: 'liveness',
            httpGet: {
              path: '/healthz',
              port: 3000,
            },
            failureThreshold: 5,
            initialDelaySeconds: 60,
            periodSeconds: 3,
            successThreshold: 1,
            timeoutSeconds: 3,
          },
          {
            type: 'readiness',
            httpGet: {
              path: '/healthz',
              port: 3000,
            },
            failureThreshold: 2,
            initialDelaySeconds: 30,
            periodSeconds: 3,
            successThreshold: 1,
            timeoutSeconds: 2,
          },
          {
            type: 'startup',
            httpGet: {
              path: '/healthz',
              port: 3000,
            },
            failureThreshold: 10,
            initialDelaySeconds: 3,
            periodSeconds: 6,
            successThreshold: 1,
            timeoutSeconds: 1,
          }
        ],
        resources: {
          cpu: ConfigWrapper.containerAppSubwayCpu,
          memory: ConfigWrapper.containerAppSubwayMemory,
        },
      }],
      scale: {
        maxReplicas: ConfigWrapper.containerAppSubwayMaxReplicas,
        minReplicas: ConfigWrapper.containerAppSubwayMinReplicas,
      },
    },
  }, {
    protect: true,
  });
}

function createWorkerContainerApp(
  resourceGroup: azure.resources.ResourceGroup,
  managedEnvironment: azure.app.ManagedEnvironment,
  registry: azure.containerregistry.Registry,
  registryManagedIdentity: azure.managedidentity.UserAssignedIdentity,
) {
  const containerAppWorkerName = getResourceName(Resource.CONTAINER_APP_WORKER)
  new azure.app.ContainerApp(containerAppWorkerName, {
    configuration: {
      activeRevisionsMode: 'Single',
      registries: [{
        identity: registryManagedIdentity.id,
        server: registry.loginServer,
      }],
      secrets: [
        {
          name: 'doppler-token',
          value: ' ', // Update this value in the portal once the resource is created.
        },
      ],
    },
    containerAppName: containerAppWorkerName,
    identity: {
      type: 'UserAssigned',
      userAssignedIdentities: [registryManagedIdentity.id],
    },
    environmentId: managedEnvironment.id,
    resourceGroupName: resourceGroup.name,
    template: {
      containers: [{
        command: [
          'doppler',
          'run',
          '--',
          'npm',
          'run',
          'start:worker',
        ],
        image: pulumi.interpolate`${registry.loginServer}/subway:initial`,
        env: [{
          name: 'DOPPLER_TOKEN',
          secretRef: 'doppler-token',
        }],
        name: 'subway',
        resources: {
          cpu: ConfigWrapper.containerAppWorkerCpu,
          memory: ConfigWrapper.containerAppWorkerMemory,
        },
      }],
      scale: {
        maxReplicas: ConfigWrapper.containerAppWorkerMinReplicas,
        minReplicas: ConfigWrapper.containerAppWorkerMaxReplicas,
      },
    },
  }, {
    protect: true,
  });
}
