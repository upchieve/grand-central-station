import * as azure from '@pulumi/azure-native'
import * as pulumi from '@pulumi/pulumi'

export enum Resource {
  APPLICATION_GATEWAY = 'application-gateway',
  CERTIFICATE = 'certificate',
  CONTAINER_APP_SUBWAY = 'container-app-subway',
  CONTAINER_APP_WORKER = 'container-app-worker',
  IDENTITY_SECRET_USER = 'identity-secret-user',
  KEY_VAULT = 'key-vault',
  MANAGED_ENVIRONMENT = 'managed-environment',
  MANAGED_ENVIRONMENT_SUBNET = 'managed-environment-subnet',
  PRIVATE_DNS_ZONE = 'pdns',
  PUBLIC_IP = 'public-ip',
  REGISTRY = 'registry',
  REGISTRY_ROLE_ASSIGNMENT = 'registry-role-assignment',
  REGISTRY_MANAGED_IDENTITY = 'registry-managed-identity',
  ROLE_ASSIGNMENT_SECRET_USER = 'role-assignment-secret-user',
  RESOURCE_GROUP = 'resource-group',
  STATIC_WEB_APP = 'static-web-app',
  VIRTUAL_NETWORK = 'virtual-network',
  VIRTUAL_NETWORK_SUBNET_DEFAULT = 'default',
}

export function getResourceName(resourceType: Resource) {
  if (resourceType === Resource.REGISTRY) {
    // Container Registry names must be globally unique and cannot contain dashes.
    return 'uc' + ConfigWrapper.environmentName + resourceType
  }

  let prefix = ''
  if (resourceType === Resource.KEY_VAULT) {
    // Key Vault names must be globally unique.
    prefix = 'uc-'
  }
  return prefix + ConfigWrapper.environmentName + '-' + resourceType
}

export class ConfigWrapper {
  private static config = new pulumi.Config()
  private static clientConfig = azure.authorization.getClientConfigOutput()

  static get subscriptionId() {
    return this.clientConfig.subscriptionId
  }

  static get tenantId() {
    return this.clientConfig.tenantId
  }

  static get acrPullRoleDefinition() {
    return azure.authorization.RoleDefinition.get('AcrPull', '/providers/Microsoft.Authorization/roleDefinitions/7f951dda-4ed3-4680-a7ca-43fe172d538d')
  }

  static get secretsUserRoleDefinition() {
    return azure.authorization.RoleDefinition.get('Key Vault Secrets User', '/providers/Microsoft.Authorization/roleDefinitions/4633458b-17de-408a-b874-0445c86b69e6')
  }

  static get environmentName() {
    return pulumi.getStack()
  }

  static get appURL() {
    const subdomain = this.config.get('subdomain') ?? this.environmentName
    return 'https://' + subdomain + '.upchieve.org'
  }

  static get containerRegistrySku() {
    return this.config.require('container-registry-sku')
  }

  static get managedEnvironmentAppLogsDestination() {
    return this.config.require('managed-environment-app-logs-destination')
  }

  static get managedEnvironmentZoneRedundant() {
    return this.config.requireBoolean('managed-environment-zone-redundant')
  }

  static get containerAppSubwayCpu() {
    return parseFloat(this.config.require('container-app-subway-cpu'))
  }

  static get containerAppSubwayMemory() {
    return this.config.require('container-app-subway-memory')
  }

  static get containerAppSubwayMinReplicas() {
    return this.config.requireNumber('container-app-subway-min-replicas')
  }

  static get containerAppSubwayMaxReplicas() {
    return this.config.requireNumber('container-app-subway-max-replicas')
  }

  static get containerAppWorkerCpu() {
    return parseFloat(this.config.require('container-app-worker-cpu'))
  }

  static get containerAppWorkerMemory() {
    return this.config.require('container-app-worker-memory')
  }

  static get containerAppWorkerMinReplicas() {
    return this.config.requireNumber('container-app-worker-min-replicas')
  }

  static get containerAppWorkerMaxReplicas() {
    return this.config.requireNumber('container-app-worker-max-replicas')
  }

  static get staticWebAppCdn() {
    return this.config.require('static-web-app-cdn')
  }

  static get staticWebAppSku() {
    return this.config.require('static-web-app-sku')
  }

  static get applicationGatewaySku() {
    return this.config.require('application-gateway-sku')
  }

  static get applicationGatewayMaxCapacity() {
    return this.config.requireNumber('application-gateway-max-capacity')
  }

  static get applicationGatewayMinCapacity() {
    return this.config.requireNumber('application-gateway-min-capacity')
  }
}
