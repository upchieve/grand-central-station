import * as azure from '@pulumi/azure-native'
import { createHighLine } from './high-line'
import { createSubway } from './subway'
import { createNetworking } from './networking'
import { getResourceName, Resource } from './utils'

const resourceGroupName = getResourceName(Resource.RESOURCE_GROUP)
const resourceGroup = new azure.resources.ResourceGroup(resourceGroupName, {
  resourceGroupName: resourceGroupName,
}, {
  protect: true,
})

const virtualNetworkName = getResourceName(Resource.VIRTUAL_NETWORK)
const virtualNetwork = new azure.network.VirtualNetwork(virtualNetworkName, {
  addressSpace: {
    addressPrefixes: [
      '10.0.0.0/16',
    ]
  },
  resourceGroupName: resourceGroup.name,
  virtualNetworkName: virtualNetworkName,
}, {
  protect: true,
})

const { managedEnvironment, subwayContainerApp } = createSubway(resourceGroup, virtualNetwork)
const staticSite = createHighLine(resourceGroup)
createNetworking(resourceGroup, virtualNetwork, managedEnvironment, subwayContainerApp, staticSite)
