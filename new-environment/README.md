# new-environment Pulumi Project

This Pulumi project contains the code for setting up a new app environment.

For a more detailed description of the cloud architecture, see [here](https://almanac.io/folders/engineering-uCbd0I/cloud-architecture-Dvf2DI4HxcGMv7eu0N5ct4iDwQUZp3Kl?docView=Commenting).

The following are the environments configured:
* prod - the production app environment.
* staging - the "closest to production" environment. The config should match prod - the only thing that doesn't is the load balancer because it's very expensive. If you need to test something with the load balancer against prod config, you can update the load balancer temporarily from Azure Portal.
* dev - engineer's playground environment. For legacy reasons, you'll sometimes see portions of this referred to as "hackers" (like the databases).

Each engineer can set up their own playground environment as well. To do so,
follow the instructions below using the default config values. In general, the dev
configs should prioritize cost when possible, i.e. scale to 0, no zone redundancy, etc.

## Create Environment
A full environment is created from two executions of a stack, the first of which
targets only the creation of the Azure Key Vault and Container Registry. In
between the stack executions,  there are steps to generate and upload a Let's
Encrypt certificate to the Key Vault that is used by the load balancer and
upload an initial image to the Container Registry. We do this because there is a
bootstrapping problem - we can't create the Application Gateway (layer 7 load
balancer) without a certificate, and we can't deploy an Azure Container App (our
backend subway and worker) without an initial image.

Prerequisites: You will need to have and be logged into the Azure, pulumi, and Doppler CLIs.

1. Set the correct Azure Subscription.
   ```
   $ az account list # to find the id for "Microsoft Azure Sponsorship".
   $ az account set --subscription <subscription id>
   ```
2. Create the new Pulumi stack.
   - The stack name will be used as the subdomain (unless otherwise specified in the config) and as part of the naming scheme of resources.
   ```
   $ cd new-environment
   $ pulumi stack init <stack name>
   ```
3. If changing any default config values, add them into a file named `Pulumi.<stack name>.yaml`.
4. Preview and apply the changes for creating the Resource Group, Key Vault and Container Registry.
   - You will need to construct the Uniform Resource Name (URN) for the Resource Group, Key Vault, and Container Registry, and target those resources specifically.
   - To see the full format of an URN in our project, you can select another stack and run `pulumi stack --show-urns`.
   ```
   $ pulumi up \
     --target urn:pulumi:<stack name>::new-environment::azure-native:resources:ResourceGroup::<stack name>-resource-group \
     --target urn:pulumi:<stack name>::new-environment::azure-native:keyvault:Vault::uc-<stack name>-key-vault \
     --target urn:pulumi:<stack name>::new-environment::azure-native:containerregistry:Registry::uc<stack name>registry
   ```
   - You will see an error about the `azure-native:keyvault:Secret` not existing. You can safely ignore this - we are creating the certificate in the next step! As of writing, Pulumi doesn't have a way ([yet](https://github.com/pulumi/pulumi/issues/9346)) to ignore a target, so trying to retrieve a non-existent resource unsurprisingly fails, but it doesn't affect creation of the resources we just targeted.
   - You can now see your Resource Group, with the Key Vault and Container Registry, in the Azure Portal!
5. Follow instructions for generating and uploading a Let's
Encrypt certificate below. 
6. Build our subway backend and push an image to the Container Registry.
   ```
   $ az acr login --name uc<stack name>registry.azurecr.io
   $ cd subway # You will need to execute the following commands from the subway root directory.
   $ docker build --tag uc<stack name>registry.azurecr.io/subway:initial . # Note: the period at the end is important!
   $ docker push uc<stack name>registry.azurecr.io/subway:initial
   ```
7. Preview and apply the rest of the changes, which includes setting up the load balancer with the certificate uploaded in the previous step.
   ```
   $ cd grand-central-station/new-environment # go back to this directory.
   $ pulumi up
   ```
   - Don't be surprised that some resources take a very long time to create - for example, the Managed Environment and Application Gateway can take upwards of 5 minutes each.
   - Once the command completes, you will see all your resources in the Azure Portal!
   - However, the environment is not functional yet.
8. Redeploy subway with the correct Doppler token.
   - The Container App already has the right image, but it needs a correct Doppler Token to start up. 
   - In Doppler UI, create a new config branched off of subway staging (if you are setting up your personal environment, otherwise skip).
      - Update any values as necessary. Specifically, you will need to update the subdomain of URLs to match the certificate domain and (maybe) the sockets port to 3000 (aka the app's default exposed port): `SUBWAY_CLIENT_HOST`, `SUBWAY_SERVER_HOST`, `SUBWAY_SOCKETS_PORT`.
      - Select the "ACCESS" tab, then generate a new service token. Copy the generated token and store it in 1Password - you will not get it again!
   - Add the Doppler token as a secret environment variable for the Container Apps through the Azure portal.
      - For both subway and the worker, go to their Container App view, select "Secrets" in the side menu, and update `doppler-token`.
   - Restart the containers through the Azure portal.
      - For both subway and the worker, go to their Container App view, select "Revisions and replicas", click the active revision, then click "Restart".
9. Deploy high-line with the correct Doppler token.
   - In Doppler UI, create a new config branched off of high-line hackers (if you are setting up your personal environment, otherwise skip).
      - Update any values as necessary. Specifically, you will need to update the subdomain of URLs to match the certificate domain.
      - Select the "ACCESS" tab, then generate a new service token. Copy the generated token and store it in 1Password - you will not get it again!
   - Add the token to your 1Password personal vault with the name `doppler-high-line`.
   - Go to high-line root directory to build and deploy with the following npm script:
   ```
   $ npm run deploy:local
   ```
10. Lastly, add the DNS record.
   - Copy the public IP of the Application Gateway from the Azure portal.
   - If this is for your personal environment: In Cloudflare, add a DNS A record, with your subdomain as the name, and the IP address as the content.
   - If this is for an existing environment: In Cloudflare, update the DNS for the subdomain.

Congrats! You're all set up and can now go to your app in the browser.

..However, there are still some services - PostHog(?), reCAPTCHA, SSO, etc. - that won't work until you add your subdomain as an authorized URL. Reach out to alex.lindsay@ if you want those services and need help adding your subdomain to them.

### Let's Encrypt Certificate Generation
As of writing, Let's Encrypt is not one of Azure's partnered Certificate Authorities.
That means we have to generate and upload the certificate ourselves.
See [Creating a certificate with a CA not partnered with Key Vault](https://learn.microsoft.com/en-us/azure/key-vault/certificates/certificate-scenarios#creating-a-certificate-with-a-ca-not-partnered-with-key-vault).

#### Production Environment
If creating the certificate for a production environment, there are some
additional steps to perform before the rest below, so that the certificates are never on an engineer's machine.

1. Go to the Azure Portal and open Cloud Shell.
2. Create a virtual machine.
   The next few steps are pulled from the [Azure Quickstart](https://learn.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-cli)
   ```
   $ export RANDOM_ID="$(openssl rand -hex 3)"
   $ export VM_NAME="vm$RANDOM_ID"
   $ export RESOURCE_GROUP=<resource group name> # TODO: Try in a different resource group than the key vault, so can simply delete that resource group at the end.
   $ export USERNAME=azureuser
   $ export VM_IMAGE="Canonical:0001-com-ubuntu-minimal-jammy:minimal-22_04-lts-gen2:latest"
   $ az vm create \
      --resource-group $RESOURCE_GROUP \
      --name $VM_NAME \
      --image $VM_IMAGE \
      --admin-username $USERNAME \
      --assign-identity \
      --generate-ssh-keys \
      --public-ip-sku Standard \
      --data-delete-option delete \
      --nic-delete-option delete \
      --os-disk-delete-option delete
   ```
3. From the Cloud Shell, SSH into the VM.
   ```
   $ export IP_ADDRESS=$(az vm show --show-details --resource-group $RESOURCE_GROUP --name $VM_NAME --query publicIps --output tsv)
   $ ssh -o StrictHostKeyChecking=no $USERNAME@$IP_ADDRESS
   ```
4. Install certbot, and install and log into the Azure CLI.
   ```
   $ sudo snap install certbot
   $ curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
   $ az login
   ```
5. Follow the instructions below, but instead of running from your local machine in this directory, run the commands in the VM.
6. Delete the VM and any other resources that were created to support it, and remove your Key Vault Administrator role.

#### All Environments
Prerequisites: You will need to have certbot installed.

1. Update the CN in the subject field in `upchieve-cert-request.json` to the domain name this certificate will be registered for.
   - The file is used for the certificate policy - so the domain needs to match.
2. Assign yourself the "Key Vault Administrator" role for the Key Vault in Azure Portal.
   - From the Key Vault > "Access Control (IAM)" > "Add" > "Add role assignment"
3. Create the certificate request.
   ```
   $ cd grand-central-station/new-environment
   $ az keyvault certificate create \
      --vault-name uc-<stack name>-key-vault \
      --name <stack name>-certificate \
      --policy @upchieve-cert-request.json
   ```
   - You can ignore the `'NoneType' object has no attribute 'lower'` output.
4. Get the certificate signing request from the certificate request.
   ```
   $ bash -c \
       'echo "-----BEGIN CERTIFICATE REQUEST-----" &&
       az keyvault certificate pending show \
           --vault-name uc-<stack name>-key-vault \
           --name <stack name>-certificate \
           --query csr -o tsv &&
       echo "-----END CERTIFICATE REQUEST-----"' > ./up-cert.csr
   ```
5. Submit the certificate signing request to Let's Encrypt.
   ```
   $ sudo certbot certonly \
      --preferred-challenge dns \
      --manual \
      --csr ./up-cert.csr
   ```
   - There will be a bunch of output and an option to Continue - Do not press enter yet!
6. Add a DNS TXT record to Cloudflare to prove ownership of the domain.
   - In the output from the previous command, find the DNS TXT record name and unique value, and add it in Cloudflare.
7. Wait for the DNS settings to propagate.
   - We want to make sure that TXT record you created in the previous step is deployed before we ask Let's Encrypt to verify, and this can take a few minutes.
   In a NEW terminal window, you can check the status using the DNS lookup utility.
   ```
   $ dig -t txt _acme-challenge.<stack name>.upchieve.org
   ```
   - Once you see the value you had added in Cloudflare, you can press enter in the certbot terminal, and you will receive the certificates from Let's Encrypt.
8. Merge the full chain certificate from Let's Encrypt with the pending certificate request in the Key Vault.
   - Note: Double check the full chain name in the command below matches the one just generated for you.
   ```
   $ az keyvault certificate pending merge \
       --vault-name uc-<stack name>-key-vault \
       --name <stack name>-certificate \
       --file ./0001_chain.pem
   ```
9. Create a calendar event to renew the certificate set 2 weeks prior to certificate expiration, and verify the certificate in Azure has the correct expiration date.
10. Remove your role as a Key Vault Administrator.

Note: Sometimes `az` CLI commands hang on my local machine. When that happens, I use the
Azure Docker container. If this happens to you, just let me (alex.lindsay@) know and I can
pair with you.

## Update Environment
1. Set the correct Azure Subscription.
   ```
   $ az account list # to find the id for "Microsoft Azure Sponsorship"
   $ az account set --subscription <subscription id>
   ```
2. Select the correct stack.
   ```
   $ pulumi select <stack name>
   ```
3. If necessary, add/update any environment specific configs in the `Pulumi.<stack name>.yaml` file.
   ```
   $ pulumi config set <key> <value>
   ```
4. Preview the changes.
   ```
   $ pulumi preview
   ```
5. If all looks good, apply the changes.
   ```
   $ pulumi up
   ```
