import * as pulumi from '@pulumi/pulumi'
import * as azure from '@pulumi/azure-native'
import { getResourceName, Resource, ConfigWrapper } from './utils'

export function createNetworking(
  resourceGroup: azure.resources.ResourceGroup,
  virtualNetwork: azure.network.VirtualNetwork,
  managedEnvironment: azure.app.ManagedEnvironment,
  subwayContainerApp: azure.app.ContainerApp,
  staticSite: azure.web.StaticSite,
) {
  // Certificate Management.
  const keyVault = createKeyVault(resourceGroup)
  const secretsUserIdentity = createSecretsUserManagedIdentity(resourceGroup, keyVault)
  const certificate = getCertificate(resourceGroup, keyVault)

  createPrivateDnsZone(
    resourceGroup,
    virtualNetwork,
    managedEnvironment
  )
  const subnet = createSubnet(resourceGroup, virtualNetwork)
  const publicIp = createPublicIp(resourceGroup)
  createApplicationGateway(
    resourceGroup,
    subnet,
    managedEnvironment,
    subwayContainerApp,
    staticSite,
    secretsUserIdentity,
    certificate,
    publicIp
  )
}

function createKeyVault(
  resourceGroup: azure.resources.ResourceGroup,
) {
  const keyVaultName = getResourceName(Resource.KEY_VAULT)
  return new azure.keyvault.Vault(keyVaultName, {
    properties: {
      enablePurgeProtection: true,
      enableRbacAuthorization: true,
      enableSoftDelete: true,
      sku: {
        family: 'A',
        name: azure.keyvault.SkuName['Standard'],
      },
      softDeleteRetentionInDays: 90,
      tenantId: ConfigWrapper.tenantId,
    },
    resourceGroupName: resourceGroup.name,
    vaultName: keyVaultName,
  }, {
    protect: true,
  });
}

function createSecretsUserManagedIdentity(
  resourceGroup: azure.resources.ResourceGroup,
  keyVault: azure.keyvault.Vault,
) {
  const managedIdentityName = getResourceName(Resource.IDENTITY_SECRET_USER)
  const managedIdentity = new azure.managedidentity.UserAssignedIdentity(managedIdentityName, {
    resourceGroupName: resourceGroup.name,
    resourceName: managedIdentityName,
  }, {
    protect: true,
  });

  const roleAssignmentName = getResourceName(Resource.ROLE_ASSIGNMENT_SECRET_USER)
  new azure.authorization.RoleAssignment(roleAssignmentName, {
    principalId: managedIdentity.principalId,
    principalType: 'ServicePrincipal',
    roleDefinitionId: ConfigWrapper.secretsUserRoleDefinition.id,
    scope: keyVault.id,
  }, {
    protect: true,
  });

  return managedIdentity
}

function getCertificate(
  resourceGroup: azure.resources.ResourceGroup,
  keyVault: azure.keyvault.Vault
) {
  const certificateName = getResourceName(Resource.CERTIFICATE)
  const certificateId = pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.KeyVault/vaults/${keyVault.name}/secrets/${certificateName}`
  return azure.keyvault.Secret.get(
    certificateName,
    certificateId
  )
}

function createPrivateDnsZone(
  resourceGroup: azure.resources.ResourceGroup,
  virtualNetwork: azure.network.VirtualNetwork,
  managedEnvironment: azure.app.ManagedEnvironment,
) {
  const zoneName = getResourceName(Resource.PRIVATE_DNS_ZONE)
  const zone = new azure.network.PrivateZone(zoneName, {
    location: 'Global',
    privateZoneName: managedEnvironment.defaultDomain,
    resourceGroupName: resourceGroup.name
  }, {
    protect: true,
  })

  const vnetLinkName = 'vnet-pdns-link'
  new azure.network.VirtualNetworkLink(vnetLinkName, {
    location: 'Global',
    privateZoneName: zone.name,
    registrationEnabled: false,
    resourceGroupName: resourceGroup.name,
    virtualNetwork: {
      id: virtualNetwork.id,
    },
    virtualNetworkLinkName: vnetLinkName,
  }, {
    protect: true,
  })

  new azure.network.PrivateRecordSet('record-set-star', {
    aRecords: [{
      ipv4Address: managedEnvironment.staticIp,
    }],
    privateZoneName: zone.name,
    recordType: 'A',
    relativeRecordSetName: '*',
    resourceGroupName: resourceGroup.name,
    ttl: 3600,
  }, {
    protect: true,
  })

  new azure.network.PrivateRecordSet('record-set-at', {
    aRecords: [{
      ipv4Address: managedEnvironment.staticIp,
    }],
    privateZoneName: zone.name,
    recordType: 'A',
    relativeRecordSetName: '@',
    resourceGroupName: resourceGroup.name,
    ttl: 3600,
  }, {
    protect: true,
  })

  return zone
}

function createSubnet(
  resourceGroup: azure.resources.ResourceGroup,
  virtualNetwork: azure.network.VirtualNetwork,
) {
  const subnetName = getResourceName(Resource.VIRTUAL_NETWORK_SUBNET_DEFAULT)
  return new azure.network.Subnet(subnetName, {
    addressPrefix: '10.0.0.0/23',
    resourceGroupName: resourceGroup.name,
    subnetName: 'default',
    virtualNetworkName: virtualNetwork.name
  })
}

function createPublicIp(
  resourceGroup: azure.resources.ResourceGroup,
) {
  const publicIpName = getResourceName(Resource.PUBLIC_IP)
  return new azure.network.PublicIPAddress(publicIpName, {
    publicIPAddressVersion: 'IPv4',
    publicIPAllocationMethod: 'Static',
    publicIpAddressName: publicIpName,
    resourceGroupName: resourceGroup.name,
    sku: {
      name: 'Standard',
      tier: 'Regional',
    },
  }, {
    protect: true,
  });
}

function createApplicationGateway(
  resourceGroup: azure.resources.ResourceGroup,
  subnet: azure.network.Subnet,
  managedEnvironment: azure.app.ManagedEnvironment,
  subwayContainerApp: azure.app.ContainerApp,
  staticSite: azure.web.StaticSite,
  secretsUserIdentity: azure.managedidentity.UserAssignedIdentity,
  certificate: azure.keyvault.Secret,
  publicIp: azure.network.PublicIPAddress,
) {
  const applicationGatewayName = getResourceName(Resource.APPLICATION_GATEWAY)
  const highLineBackendPoolName = 'high-line-backend-pool'
  const subwayBackendPoolName = 'subway-backend-pool'
  const highLineBackendSettingsName = 'high-line-backend-settings'
  const subwayBackendSettingsName = 'subway-backend-settings'
  const publicIpConfigName = 'public-ip-config'
  const port443Name = 'port-443'
  const port80Name = 'port-80'
  const httpsListenerName = 'https-listener'
  const httpListenerName = 'http-listener'
  const httpRedirectRuleName = 'http-redirect-rule'
  const httpRequestRoutingRuleName = 'http-request-routing-rule'
  const highLineProbeName = 'high-line-health-probe'
  const subwayProbeName = 'subway-health-probe'
  const urlPathMapName = 'url-path-map'
  const FIVE_MINUTES_IN_SECONDS = 5 * 60
  const ONE_MINUTE_IN_SECONDS = 60
  const TWENTY_SECONDS = 20

  new azure.network.ApplicationGateway(applicationGatewayName, {
    applicationGatewayName,
    autoscaleConfiguration: ConfigWrapper.applicationGatewaySku === 'Basic'
      ? undefined
      : {
        maxCapacity: ConfigWrapper.applicationGatewayMaxCapacity,
        minCapacity: ConfigWrapper.applicationGatewayMinCapacity,
    },
    backendAddressPools: [
      {
        backendAddresses: [{
          fqdn: staticSite.defaultHostname,
        }],
        name: highLineBackendPoolName,
      },
      {
        backendAddresses: [{
          // TODO: Is there a better way to get the container app domain?
          fqdn: pulumi.interpolate`${subwayContainerApp.name}.${managedEnvironment.defaultDomain}`,
        }],
        name: subwayBackendPoolName,
      },
    ],
    backendHttpSettingsCollection: [
      {
        cookieBasedAffinity: 'Disabled',
        name: highLineBackendSettingsName,
        pickHostNameFromBackendAddress: true,
        port: 443,
        probe: {
          id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/probes/${highLineProbeName}`
        },
        protocol: 'Https',
        requestTimeout: TWENTY_SECONDS,
      },
      {
        affinityCookieName: 'ApplicationGatewayAffinity',
        cookieBasedAffinity: 'Enabled',
        connectionDraining: {
          enabled: true,
          drainTimeoutInSec: ONE_MINUTE_IN_SECONDS, // TODO: Figure out value.
        },
        name: subwayBackendSettingsName,
        pickHostNameFromBackendAddress: true,
        port: 443,
        probe: {
          id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/probes/${subwayProbeName}`
        },
        protocol: 'Https',
        requestTimeout: FIVE_MINUTES_IN_SECONDS,
      },
    ],
    enableHttp2: true,
    frontendIPConfigurations: [{
      name: publicIpConfigName,
      publicIPAddress: {
        id: publicIp.id,
      },
    }],
    frontendPorts: [{
      name: port443Name,
      port: 443,
    }, {
      name: port80Name,
      port: 80,
    }],
    gatewayIPConfigurations: [{
      name: 'appgw-ip-config',
      subnet: {
        id: subnet.id
      },
    }],
    httpListeners: [{
      frontendIPConfiguration: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/frontendIPConfigurations/${publicIpConfigName}`,
      },
      frontendPort: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/frontendPorts/${port443Name}`,
      },
      name: httpsListenerName,
      protocol: 'Https',
      requireServerNameIndication: false,
      sslCertificate: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/sslCertificates/${certificate.name}`,
      },
    },
    {
      frontendIPConfiguration: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/frontendIPConfigurations/${publicIpConfigName}`,
      },
      frontendPort: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/frontendPorts/${port80Name}`,
      },
      name: httpListenerName,
      protocol: 'Http',
      requireServerNameIndication: false,
    }],
    identity: {
      type: 'UserAssigned',
      userAssignedIdentities: [secretsUserIdentity.id],
    },
    probes: [{
      interval: 30, // TODO: Increase for dev envs?
      name: highLineProbeName,
      path: '/',
      pickHostNameFromBackendHttpSettings: true,
      protocol: 'Https',
      timeout: 30,
      unhealthyThreshold: 1,
    },
    {
      interval: 30, // TODO: Increase for dev envs?
      name: subwayProbeName,
      path: '/healthz',
      pickHostNameFromBackendHttpSettings: true,
      protocol: 'Https',
      timeout: 30,
      unhealthyThreshold: 1,
    }],
    redirectConfigurations: [
      {
        includePath: true,
        includeQueryString: true,
        name: httpRedirectRuleName,
        redirectType: 'Found',
        requestRoutingRules: [{
          id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/requestRoutingRules/${httpRequestRoutingRuleName}`
        }],
        targetListener: {
          id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/httpListeners/${httpsListenerName}`
        }
      }
    ],
    requestRoutingRules: [{
      httpListener: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/httpListeners/${httpsListenerName}`,
      },
      name: 'https-request-routing-rule',
      priority: 1,
      ruleType: 'PathBasedRouting',
      urlPathMap: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/urlPathMaps/${urlPathMapName}`,
      },
    }, {
        httpListener: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/httpListeners/${httpListenerName}`,
        },
      name: httpRequestRoutingRuleName,
      priority: 2,
      ruleType: 'Basic',
      redirectConfiguration: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/redirectConfigurations/${httpRedirectRuleName}`,
      }
    }],
    resourceGroupName: resourceGroup.name,
    sku: {
      name: ConfigWrapper.applicationGatewaySku,
      tier: ConfigWrapper.applicationGatewaySku,
    },
    sslCertificates: [{
      keyVaultSecretId: certificate.properties.secretUri,
      name: certificate.name,
    }],
    urlPathMaps: [{
      defaultBackendAddressPool: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/backendAddressPools/${highLineBackendPoolName}`,
      },
      defaultBackendHttpSettings: {
        id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/backendHttpSettingsCollection/${highLineBackendSettingsName}`,
      },
      name: urlPathMapName,
      pathRules: [
        {
          backendAddressPool: {
            id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/backendAddressPools/${subwayBackendPoolName}`,
          },
          backendHttpSettings: {
            id: pulumi.interpolate`/subscriptions/${ConfigWrapper.subscriptionId}/resourceGroups/${resourceGroup.name}/providers/Microsoft.Network/applicationGateways/${applicationGatewayName}/backendHttpSettingsCollection/${subwayBackendSettingsName}`,
          },
          name: 'subway-path-rule',
          paths: [
            '/api/*',
            '/auth/*',
            '/api-public/*',
            '/whiteboard/*',
            '/twiml/*',
            '/socket.io/*',
            '/healthz',
          ],
        },
      ],
    }],
    // TODO: Add zones.
    zones: [],
  }, {
    protect: true,
  });
}
