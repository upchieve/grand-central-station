import * as pulumi from '@pulumi/pulumi';
import * as azure from '@pulumi/azure-native'

enum Resource {
  DEPLOYMENT_MANAGED_IDENTITY = 'deployment-managed-identity',
  DEPLOYMENT_ROLE = 'deployment-role',
  GITLAB_FEDERATED_CREDENTIAL = 'gitlab-federated-credential',
  RESOURCE_GROUP = 'resource-group',
  ROLE_ASSIGNMENT_DEPLOYMENT = 'role-assignment-deployment',
}
function getResourceName(resourceType: Resource) {
  const environmentName = pulumi.getStack()
  return environmentName + '-' + resourceType
}

const azureConfig = azure.authorization.getClientConfigOutput()
const subscriptionId = azureConfig.subscriptionId
const resourceGroupName = getResourceName(Resource.RESOURCE_GROUP)
const scope = pulumi.interpolate`/subscriptions/${subscriptionId}/resourceGroups/${resourceGroupName}`

const deploymentRoleName = getResourceName(Resource.DEPLOYMENT_ROLE)
const deploymentRole = new azure.authorization.RoleDefinition(deploymentRoleName, {
    assignableScopes: [
      scope
    ],
    permissions: [{
      actions: [
        'microsoft.app/containerapps/write',
        'microsoft.app/containerapps/read',
        'Microsoft.ContainerRegistry/registries/pull/read',
        'Microsoft.ContainerRegistry/registries/push/write'
      ]
    }],
    roleName: deploymentRoleName,
    scope
}, {
  protect: true,
});

const managedIdentityName = getResourceName(Resource.DEPLOYMENT_MANAGED_IDENTITY)
const managedIdentity = new azure.managedidentity.UserAssignedIdentity(managedIdentityName, {
  resourceGroupName,
  resourceName: managedIdentityName,
}, {
  protect: true,
});

const roleAssignmentName = getResourceName(Resource.ROLE_ASSIGNMENT_DEPLOYMENT)
new azure.authorization.RoleAssignment(roleAssignmentName, {
  principalId: managedIdentity.principalId,
  principalType: 'ServicePrincipal',
  roleDefinitionId: deploymentRole.id,
  scope
}, {
  protect: true,
});

const gitlabFederatedCredentialName = getResourceName(Resource.GITLAB_FEDERATED_CREDENTIAL)
new azure.managedidentity.FederatedIdentityCredential(gitlabFederatedCredentialName , {
    audiences: ['api://AzureADTokenExchange'],
    federatedIdentityCredentialResourceName: gitlabFederatedCredentialName,
    issuer: 'https://gitlab.com',
    resourceGroupName,
    resourceName: managedIdentity.name,
    subject: 'project_path:upchieve/subway:ref_type:branch:ref:main',
}, {
  protect: true
});
