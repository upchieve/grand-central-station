# GitLab CI Configuration
We are using a federated identity workload to deploy subway from CI.
This approach allows us to securely authenticate and authorize access to Azure
resources without the need for managing and storing sensitive credentials (i.e.
passwords) in GitLab, thereby eliminating the risk of credential leakage and unauthorized access.

## How It Works
At a super basic level, we first establish a trust relationship between Azure
(the identity provider) and GitLab (the service provider) - which is done in this pulumi project with a `pulumi up`.
Then, when we request to login to Azure within the CI pipeline, Azure provides a short-lived access token
that grants the necessary permissions to interact with the specified Azure resources.

For more detailed explanation of the federated identity workload flow and the protocols involved, check out the following documentation:
- (Azure) [Workload identity federation](https://learn.microsoft.com/en-us/entra/workload-id/workload-identity-federation)
- (GitLab) [Connect to cloud services](https://docs.gitlab.com/ee/ci/cloud_services/index.html)
- (GitLab) [Configure OpenID Connect in Azure to retrieve temporary credentials](https://docs.gitlab.com/ee/ci/cloud_services/azure/)

To setup, all you have to do is select the appropriate stack and run `pulumi up`.
In the CI job, you can login to Azure within `before_script` with the following:
`az login --service-principal --username <the managed identity ID we create in this project with the necessary permissions to access the resources we need> --tenant <the subscription's tenant ID> --federated-token`.
You'll also need to include the following options on the job (check out the GitLab documentation above for more info):
```
id_tokens:
  GITLAB_OIDC_TOKEN:
    aud: api://AzureADTokenExchange
```

