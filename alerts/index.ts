import * as newrelic from "@pulumi/newrelic";
import * as pulumi from "@pulumi/pulumi";

const config = new pulumi.Config("newrelic")
const NR_ACCOUNT_ID = config.requireSecretNumber("accountId")

const subwayLatencyPolicy = new newrelic.AlertPolicy("Subway Latency Policy", {
  accountId: NR_ACCOUNT_ID,
  name: "Subway latency policy",
}, {
  protect: true,
});

new newrelic.NrqlAlertCondition("Subway Production Web Transaction High Response Time", {
  accountId: NR_ACCOUNT_ID,
  aggregationDelay: "120",
  aggregationMethod: "event_flow",
  aggregationWindow: 300,
  critical: {
    operator: "above",
    threshold: 400,
    thresholdDuration: 600,
    thresholdOccurrences: "all",
  },
  fillOption: "none",
  name: "Subway Production Web Transction High Response Time",
  nrql: {
    query: `SELECT average(apm.service.transaction.duration) * 1000 AS 'Response time (ms)'
            FROM Metric
            WHERE appName = 'subway-production'
              AND transactionName NOT LIKE '%socket-io%'
              AND transactionType = 'Web'
    `,
  },
  policyId: subwayLatencyPolicy.id.apply(id => parseInt(id, 10)),
  violationTimeLimit: "TWENTY_FOUR_HOURS",
  warning: {
    operator: "above",
    threshold: 300,
    thresholdDuration: 1800,
    thresholdOccurrences: "all",
  },
}, {
  protect: true,
});

new newrelic.NrqlAlertCondition("Subway Production Other Transaction High Response Time", {
  accountId: NR_ACCOUNT_ID,
  aggregationDelay: "120",
  aggregationMethod: "event_flow",
  aggregationWindow: 120,
  critical: {
    operator: "above",
    threshold: 2000,
    thresholdDuration: 240,
    thresholdOccurrences: "all",
  },
  fillOption: "none",
  name: "Subway Production Other Transaction High Response Time",
  nrql: {
    query: `SELECT average(apm.service.transaction.duration) * 1000 AS 'Response time (ms)' 
            FROM Metric 
            WHERE appName = 'subway-production' 
              AND transactionType = 'Other'
    `,
  },
  policyId: subwayLatencyPolicy.id.apply(id => parseInt(id, 10)),
  slideBy: 30,
  violationTimeLimit: "TWENTY_FOUR_HOURS",
}, {
  protect: true,
});
