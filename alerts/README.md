# UPchieve Alerts _In Progress_
Alert policies and alert conditions set in New Relic.

This Pulumi project is in progress. It is acting as a template for us
to continue importing our alert policies and conditions. For now, I have
simply imported the Subway Latency Policy, which contains the alerts we
have been changing a lot most recently.

## Adding a New Alert Condition
The New Relic UI is probably still the easiest way to add a new alert condition,
since it provides a preview chart and useful tooltips for setting threshold etc. Once created, import the alert condition here.

## Importing Existing Alert Policies and Alert Conditions
We can import existing resources using `pulumi import` CLI command, and copying the output into the Pulumi code.

### Alert Policies
`$ pulumi import newrelic:index/alertPolicy:AlertPolicy <Name for Alert Policy in Pulumi> <alert_policy_id>:<account_id>`

### Alert Conditions
The resource name depends on the type of alert condition, which you can find in the New Relic UI. For example, to import an NRQL alert condition, run the following:
`$ pulumi import newrelic:index/nrqlAlertCondition:NrqlAlertCondition <Name for Alert Condition in Pulumi> <alert_policy_id>:<alert_condition_id>`

