import { Config } from '@pulumi/pulumi'

export const config = new Config()
config.require('upchieveZoneId')
config.requireSecret('subwayStagingDopplerToken')
config.requireSecret('subwayDemoDopplerToken')
config.requireSecret('subwayProductionDopplerToken')
config.requireSecret('subwayHackersDopplerToken')
