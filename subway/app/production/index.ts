import * as gcs from '../../gcs'
import { AivenCloudNames, AivenPlans, createSubwayApp } from '../factory'
import { Namespaces } from '../project'
import { config } from '../../config'

export const productionEnvironment = createSubwayApp(
  Namespaces.PRODUCTION,
  'app',
  {
    create: true,
    plan: AivenPlans.STARTUP_4,
    cloudName: AivenCloudNames.DEFAULT,
    project: gcs.aivenProductionProjectId,
    terminationProtection: true
  },
  {
    create: true,
    plan: AivenPlans.BUSINESS_8,
    cloudName: AivenCloudNames.DEFAULT,
    project: gcs.aivenProductionProjectId,
    terminationProtection: true,
    appPassword: config.requireSecret('subwayProductionPgPassword'),
    staffRoPassword: config.requireSecret('subwayProductionStaffPassword'),
    retoolPassword: config.requireSecret('subwayProductionRetoolPassword'),
    metrics: true,
    logs: true
  },
)
