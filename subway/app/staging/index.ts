import * as gcs from '../../gcs'
import { AivenCloudNames, AivenPlans, createSubwayApp } from '../factory'
import { Namespaces } from '../project'
import { config } from '../../config'

export const stagingEnvironment = createSubwayApp(
  Namespaces.STAGING,
  Namespaces.STAGING,
  {
    create: true,
    plan: AivenPlans.HOBBYIST,
    cloudName: AivenCloudNames.AWS_US_EAST_1,
    project: gcs.aivenStagingProjectId,
    terminationProtection: true
  },
  {
    create: true,
    plan: AivenPlans.STARTUP_4,
    cloudName: AivenCloudNames.DEFAULT,
    project: gcs.aivenStagingProjectId,
    terminationProtection: true,
    appPassword: config.requireSecret('subwayStagingPgPassword'),
    staffRoPassword: config.requireSecret('subwayStagingStaffPassword'),
    retoolPassword: config.requireSecret('subwayStagingRetoolPassword'),
    metrics: true
  },
)
