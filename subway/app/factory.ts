import * as k8s from '@pulumi/kubernetes'
import { config } from '../config'
import * as ambassador from '../../crds/ambassador/index'
import * as certmanager from '../../crds/cert-manager/index'
import * as terminal from '../terminal'
import * as cloudflare from '@pulumi/cloudflare'
import * as argocd from '../../crds/argocd/index'
import * as aiven from '@pulumi/aiven'
import { appProject, projectName } from './project'
import { capitalCase } from 'change-case'
import { Output } from '@pulumi/pulumi'

export interface SubwayApp {
  dopplerToken: k8s.core.v1.Secret
  redis?: aiven.Redis
  pgServer?: aiven.Pg
  pgDatabase?: aiven.Database
  pgAppUser?: aiven.ServiceUser
  pgStaffRoUser?: aiven.ServiceUser
  pgRetoolUser?: aiven.ServiceUser
  pgAppConnPool?: aiven.ConnectionPool
  pgRetoolConnPool?: aiven.ConnectionPool
  pgStaffConnPool?: aiven.ConnectionPool
  dns: cloudflare.Record
  certificate: certmanager.certmanager.v1.Certificate
  host: ambassador.getambassador.v3alpha1.Host
  app: argocd.argoproj.v1alpha1.Application
}

function calculateMaxConnections(plan: string) {
  if (plan === 'hobbyist') {
    return 5
  } else {
    // max conns should be cpu * 5
    const ramCount = Number(plan.slice(-1))
    const cpuCount = ramCount / 4
    return cpuCount * 5
  }
}

export enum AivenPlans {
  HOBBYIST = 'hobbyist',
  STARTUP_4 = 'startup-4',
  STARTUP_8 = 'startup-8',
  BUSINESS_4 = 'business-4',
  BUSINESS_8 = 'business-8'
}

export enum AivenCloudNames {
  // This is the cloud most of our resources are in
  DEFAULT = 'azure-eastus2',
  // This is the closest area that Aiven offers hobbyist redis plans
  AWS_US_EAST_1 = 'aws-us-east-1',
  // This is the closest area that Aiven offers hobbyist Postgres plans
  GOOGLE_US_EAST1 = 'google-us-east1'
}

export enum PoolModes {
  SESSION = 'session',
  TRANSACTION = 'transaction',
  STATEMENT = 'statement'
}

interface RedisOptions {
  create?: boolean
  plan: AivenPlans
  cloudName: AivenCloudNames
  project: Output<any>,
  terminationProtection?: boolean
  metrics?: boolean
  logs?: boolean
}


interface PostgresOptions {
  create: boolean
  plan: AivenPlans
  cloudName: AivenCloudNames
  project: Output<any>
  terminationProtection?: boolean
  pgVersion?: string
  appPassword: Output<any>
  staffRoPassword: Output<any>
  retoolPassword: Output<any>
  metrics?: boolean
  logs?: boolean
}

function getServiceIntegrations(metrics: boolean, logs: boolean) {
  const integrations = []
  if (metrics) {
    integrations.push({
      integrationType: 'prometheus',
      sourceServiceName: 'newrelic'
    })
  }
  if (logs) {
    integrations.push({
      integrationType: 'rsyslog',
      sourceServiceName: 'newrelic-logs'
    })
  }
  return integrations
}

export function createSubwayApp(
  environment: string,
  subdomain: string,
  redisOptions: RedisOptions,
  postgresOptions: PostgresOptions,
  argoTargetRevision = 'HEAD'): SubwayApp {

  const zoneId = config.get('upchieveZoneId') || 'bogus'
  const tokenEnvName = capitalCase(environment).replace(' ', '')
  const dopplerToken = new k8s.core.v1.Secret(`subway-${environment}-doppler-token`, {
    metadata: {
      name: `subway-${environment}-doppler-token`,
      namespace: environment
    },
    stringData: {
      DOPPLER_TOKEN: config.requireSecret(`subway${tokenEnvName}DopplerToken`) || 'bogus'
    },
  }, { provider: terminal.provider })

  const dns = new cloudflare.Record(`${subdomain}-upchieve-org`, {
    name: subdomain,
    zoneId,
    type: 'A',
    value: terminal.ambassadorIp,
    ttl: 1,
    proxied: true,
  })

  const certificate = new certmanager.certmanager.v1.Certificate(`${subdomain}-certificate-self-signed`, {
    metadata: {
      name: `${subdomain}-self-signed`,
      namespace: environment,
    },
    spec: {
      secretName: `${subdomain}-upchieve-org-self-signed`,
      duration: '2160h',
      renewBefore: '360h',
      dnsNames: [`${subdomain}.upchieve.org`],
      issuerRef: {
        name: 'self-signed',
        kind: 'ClusterIssuer',
      }
    }
  }, { provider: terminal.provider })

  const host = new ambassador.getambassador.v3alpha1.Host(subdomain, {
    metadata: {
      name: subdomain,
      namespace: environment,
    },
    spec: {
      hostname: `${subdomain}.upchieve.org`,
      tlsSecret: {
        name: `${subdomain}-upchieve-org-self-signed`
      },
      acmeProvider: {
        authority: 'none'
      }
    }
  }, { provider: terminal.provider, dependsOn: [ dns, certificate ] })

  const app = new argocd.argoproj.v1alpha1.Application(`subway-${environment}`, {
    metadata: {
      namespace: terminal.argocdNamespace,
      name: `subway-${environment}`,
      annotations: {
        'notifications.argoproj.io/subscribe.on-sync-suceeded.slack': 'tech-chatops',
        'notifications.argoproj.io/subscribe.on-sync-running.slack': 'tech-chatops',
        'notifications.argoproj.io/subscribe.on-sync-failed.slack': 'tech-chatops',
      }
    },
    spec: {
      destination: {
        namespace: environment,
        server: 'https://kubernetes.default.svc'
      },
      syncPolicy: {
        automated: {
          prune: true,
          selfHeal: true
        }
      },
      project: projectName,
      source: {
        path: 'subway',
        repoURL: 'https://gitlab.com/upchieve/port-authority.git',
        targetRevision: argoTargetRevision,
        helm: {
          valueFiles: [
            'values.yaml',
            `values-${environment}.yaml`,
            `a8r-values-${environment}.yaml`
          ]
        }
      }
    }
  }, { provider: terminal.provider, dependsOn: [ dopplerToken, appProject ]})

  const createdApp: SubwayApp = {
    dopplerToken,
    dns,
    certificate,
    host,
    app
  }

  if (redisOptions.create) {
    createdApp.redis = new aiven.Redis(`${environment}-redis`, {
      project: redisOptions.project,
      cloudName: redisOptions.cloudName,
      plan: redisOptions.plan,
      serviceName: 'subway',
      // this is in UTC, this is the slowest time of week for us
      maintenanceWindowDow: 'saturday',
      maintenanceWindowTime: '12:00:00',
      terminationProtection: redisOptions.terminationProtection || false,
      serviceIntegrations: redisOptions.metrics ? [{ integrationType: 'prometheus', sourceServiceName: 'newrelic' }] : []
    })
  }

  if (postgresOptions.create) {
    createdApp.pgServer = new aiven.Pg(`${environment}-postgres`, {
      project: postgresOptions.project,
      cloudName: postgresOptions.cloudName,
      plan: postgresOptions.plan,
      serviceName: 'subway-pg',
      // this is in UTC, this is the slowest time of week for us
      maintenanceWindowDow: 'saturday',
      maintenanceWindowTime: '12:00:00',
      terminationProtection: postgresOptions.terminationProtection || false,
      pgUserConfig: {
        // latest version when this was authored
        pgVersion: postgresOptions.pgVersion || '14',
        // pgbouncer is a connection pooler, which is critical for PG at scale
        // it's awesome that Aiven offers it as a server-side add-on
        pgbouncer: {},
      },
      serviceIntegrations: getServiceIntegrations(!!postgresOptions.metrics, !!postgresOptions.logs)
    })

    createdApp.pgDatabase = new aiven.Database(`${environment}-postgres-database`, {
      project: postgresOptions.project,
      serviceName: createdApp.pgServer.serviceName,
      databaseName: 'upchieve'
    })

    createdApp.pgAppUser = new aiven.ServiceUser(`${environment}-postgres-database-subway-user`, {
      project: postgresOptions.project,
      serviceName: createdApp.pgServer.serviceName,
      username: 'subway',
      password: postgresOptions.appPassword
    })

    createdApp.pgStaffRoUser = new aiven.ServiceUser(`${environment}-postgres-database-staff-read-only-user`, {
      project: postgresOptions.project,
      serviceName: createdApp.pgServer.serviceName,
      username: 'staff_ro',
      password: postgresOptions.staffRoPassword
    })

    createdApp.pgRetoolUser = new aiven.ServiceUser(`${environment}-postgres-database-retool-user`, {
      project: postgresOptions.project,
      serviceName: createdApp.pgServer.serviceName,
      username: 'retool',
      password: postgresOptions.retoolPassword
    })

    const { min, max, floor } = Math
    // this is max connections that we will allow all pgbouncer pools to create to the db
    const maxConnections = calculateMaxConnections(postgresOptions.plan)
    if (maxConnections < 3) throw new Error('max db connections is less than 3')

    // retool and staff should not get more than 5 conns total
    const retoolConnections = min(max(floor(maxConnections/5), 1), 5)
    const staffConnections = min(max(floor(maxConnections/5), 1), 5)
    // app should get the rest
    const appConnections = maxConnections - (retoolConnections + staffConnections)

    // These pools make it so the given user can never use more connections than the pool allows for
    // info on connection pooling: https://developer.aiven.io/docs/products/postgresql/concepts/pg-connection-pooling
    // a good rule for postgres is 3-5 connections per cpu
    // the pool size is the max connections the pool will make to the db
    // each pool can have up to 5000 client connections made to it
    if (environment === 'production' || environment === 'staging') {
      createdApp.pgAppConnPool = new aiven.ConnectionPool(`${environment}-postgres-app-conn-pool`, {
        project: postgresOptions.project,
        serviceName: createdApp.pgServer.serviceName,
        databaseName: createdApp.pgDatabase.databaseName,
        poolMode: PoolModes.TRANSACTION,
        poolName: 'subway',
        poolSize: appConnections,
        username: createdApp.pgAppUser.username
      })

      createdApp.pgRetoolConnPool = new aiven.ConnectionPool(`${environment}-postgres-retool-conn-pool`, {
        project: postgresOptions.project,
        serviceName: createdApp.pgServer.serviceName,
        databaseName: createdApp.pgDatabase.databaseName,
        poolMode: PoolModes.TRANSACTION,
        poolName: 'retool',
        poolSize: retoolConnections,
        username: createdApp.pgRetoolUser.username
      })

      createdApp.pgStaffConnPool = new aiven.ConnectionPool(`${environment}-postgres-staff-conn-pool`, {
        project: postgresOptions.project,
        serviceName: createdApp.pgServer.serviceName,
        databaseName: createdApp.pgDatabase.databaseName,
        poolMode: PoolModes.TRANSACTION,
        poolName: 'staff',
        poolSize: staffConnections,
        username: createdApp.pgStaffRoUser.username
      })
    }
  }

  return createdApp
}
