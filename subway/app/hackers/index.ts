import * as gcs from '../../gcs'
import { AivenCloudNames, AivenPlans, createSubwayApp } from '../factory'
import { Namespaces } from '../project'
import { config } from '../../config'

export const hackersEnvironment = createSubwayApp(
  Namespaces.HACKERS,
  Namespaces.HACKERS,
  {
    create: true,
    // hobbyist is only available in aws east1
    plan: AivenPlans.HOBBYIST,
    cloudName: AivenCloudNames.AWS_US_EAST_1,
    project: gcs.aivenHackersProjectId,
  },
  {
    create: true,
    plan: AivenPlans.HOBBYIST,
    // hobbyist is only available in google east1
    cloudName: AivenCloudNames.GOOGLE_US_EAST1,
    project: gcs.aivenHackersProjectId,
    terminationProtection: true,
    appPassword: config.requireSecret('subwayHackersPgPassword'),
    staffRoPassword: config.requireSecret('subwayHackersStaffPassword'),
    retoolPassword: config.requireSecret('subwayHackersRetoolPassword'),
  },
)
