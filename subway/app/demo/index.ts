import * as gcs from '../../gcs'
import { AivenCloudNames, AivenPlans, createSubwayApp } from '../factory'
import { Namespaces } from '../project'
import { config } from '../../config'

export const demoEnvironment = createSubwayApp(
  Namespaces.DEMO,
  Namespaces.DEMO,
  {
    create: true,
    // hobbyist is only available in aws east1
    plan: AivenPlans.HOBBYIST,
    cloudName: AivenCloudNames.AWS_US_EAST_1,
    project: gcs.aivenDemoProjectId,
  },
  {
    create: true,
    plan: AivenPlans.HOBBYIST,
    // hobbyist is only available in google east1
    cloudName: AivenCloudNames.GOOGLE_US_EAST1,
    project: gcs.aivenDemoProjectId,
    terminationProtection: true,
    appPassword: config.requireSecret('subwayDemoPgPassword'),
    staffRoPassword: config.requireSecret('subwayDemoStaffPassword'),
    retoolPassword: config.requireSecret('subwayDemoRetoolPassword'),
  },
)