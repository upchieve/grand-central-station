import { StackReference } from '@pulumi/pulumi'

const gcs = new StackReference('upchieve/grand-central-station/gcs')

export const aivenProductionProjectId = gcs.requireOutput('aivenProductionProjectId')
export const aivenStagingProjectId = gcs.requireOutput('aivenStagingProjectId')
export const aivenHackersProjectId = gcs.requireOutput('aivenHackersProjectId')
export const aivenDemoProjectId = gcs.requireOutput('aivenDemoProjectId')
export const defaultAivenCloud = gcs.requireOutput('defaultAivenCloud')
