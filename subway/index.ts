import { demoEnvironment } from './app/demo'
import { stagingEnvironment } from './app/staging'
import { productionEnvironment } from './app/production'
import { hackersEnvironment } from './app/hackers'

demoEnvironment
stagingEnvironment
productionEnvironment
hackersEnvironment
