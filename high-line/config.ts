import { Config } from '@pulumi/pulumi'

export const config = new Config()
config.requireSecret('highLineStagingDopplerToken')
config.requireSecret('highLineDemoDopplerToken')
config.requireSecret('highLineProductionDopplerToken')
config.requireSecret('highLineHackersDopplerToken')
