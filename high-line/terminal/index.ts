import { StackReference } from '@pulumi/pulumi'
import * as k8s from '@pulumi/kubernetes'

const terminal = new StackReference('upchieve/terminal/terminal')

const terminalKubeConfig = terminal.requireOutput('terminalKubeConfig')
export const ambassadorIp = terminal.requireOutput('ambassadorIp')
export const argocdNamespace = terminal.requireOutput('argocdNamespace')
export const demoNamespace = terminal.requireOutput('demoNamespace')
export const stagingNamespace = terminal.requireOutput('stagingNamespace')
export const productionNamespace = terminal.requireOutput('productionNamespace')
export const hackersNamespace = terminal.requireOutput('hackersNamespace')

export const provider = new k8s.Provider('terminal', {
  kubeconfig: terminalKubeConfig,
  suppressDeprecationWarnings: true
})
