import { createHighLineApp } from '../factory'
import { Namespaces } from '../project'

export const hackersEnvironment = createHighLineApp(
  Namespaces.HACKERS,
)
