import * as terminal from '../terminal'
import * as argocd from '../../crds/argocd/index'

export const projectName = 'high-line'

export enum Namespaces {
  DEMO = 'demo',
  STAGING = 'staging',
  PRODUCTION = 'production',
  HACKERS = 'hackers',
}

export const appProject = new argocd.argoproj.v1alpha1.AppProject('high-line', {
  metadata: {
    name: projectName,
    namespace: terminal.argocdNamespace
  },
  spec: {
    description: 'High Line app environments',
    sourceRepos: [ 'https://gitlab.com/upchieve/port-authority.git' ],
    destinations: [
      {
        namespace: Namespaces.DEMO,
        server: 'https://kubernetes.default.svc'
      },
      {
        namespace: Namespaces.STAGING,
        server: 'https://kubernetes.default.svc'
      },
      {
        namespace: Namespaces.PRODUCTION,
        server: 'https://kubernetes.default.svc'
      },
      {
        namespace: Namespaces.HACKERS,
        server: 'https://kubernetes.default.svc'
      },
    ]
  }
}, { provider: terminal.provider })
