import { createHighLineApp } from '../factory'
import { Namespaces } from '../project'

export const demoEnvironment = createHighLineApp(
  Namespaces.DEMO,
)
