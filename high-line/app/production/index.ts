import { createHighLineApp } from '../factory'
import { Namespaces } from '../project'

export const productionEnvironment = createHighLineApp(
  Namespaces.PRODUCTION,
)
