import * as k8s from '@pulumi/kubernetes'
import { config } from '../config'
import * as terminal from '../terminal'
import * as argocd from '../../crds/argocd/index'
import { appProject, projectName } from './project'
import { capitalCase } from 'change-case'

export interface HighLineApp {
  dopplerToken: k8s.core.v1.Secret
  app: argocd.argoproj.v1alpha1.Application
}


export function createHighLineApp(
  environment: string,
  argoTargetRevision= 'HEAD'
): HighLineApp {
  const tokenEnvName = capitalCase(environment).replace(' ', '')
  const dopplerToken = new k8s.core.v1.Secret(`high-line-${environment}-doppler-token`, {
    metadata: {
      name: `high-line-${environment}-doppler-token`,
      namespace: environment
    },
    stringData: {
      DOPPLER_TOKEN: config.requireSecret(`highLine${tokenEnvName}DopplerToken`) || 'bogus'
    },
  }, { provider: terminal.provider })

  const app = new argocd.argoproj.v1alpha1.Application(`high-line-${environment}`, {
    metadata: {
      namespace: terminal.argocdNamespace,
      name: `high-line-${environment}`,
      annotations: {
        'notifications.argoproj.io/subscribe.on-sync-suceeded.slack': 'tech-chatops',
        'notifications.argoproj.io/subscribe.on-sync-running.slack': 'tech-chatops',
        'notifications.argoproj.io/subscribe.on-sync-failed.slack': 'tech-chatops',
      }
    },
    spec: {
      destination: {
        namespace: environment,
        server: 'https://kubernetes.default.svc'
      },
      syncPolicy: {
        automated: {
          prune: true,
          selfHeal: true
        }
      },
      project: projectName,
      source: {
        path: 'high-line',
        repoURL: 'https://gitlab.com/upchieve/port-authority.git',
        targetRevision: argoTargetRevision,
        helm: {
          valueFiles: [
            'values.yaml',
            `values-${environment}.yaml`,
            `a8r-values-${environment}.yaml`
          ]
        }
      }
    }
  }, { provider: terminal.provider, dependsOn: [ dopplerToken, appProject ]})

  return {
    dopplerToken,
    app
  }
}
