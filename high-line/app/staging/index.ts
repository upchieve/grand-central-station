import { createHighLineApp } from '../factory'
import { Namespaces } from '../project'

export const stagingEnvironment = createHighLineApp(
  Namespaces.STAGING,
)