import { Config } from '@pulumi/pulumi'

export const config = new Config()
config.require('newRelicKey')
config.require('azureTenantId')
config.require('upchieveZoneId')
config.requireSecret('externalSecretsClientSecret')
config.requireSecret('cloudflareToken')
