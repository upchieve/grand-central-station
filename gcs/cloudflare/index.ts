import * as cloudflare from '@pulumi/cloudflare'
import { config } from '../config'
const zoneId = config.get('upchieveZoneId') || 'bogus'

// reports will go to the rua field
// dkim and spf are relaxed for now
// the policy is to reject emails that don't pass (none)
// I've been checking the reports that go to dmarc-reports@upchieve.org
// and consistently 100% of our traffic passes
// https://dmarcly.com/tools/dmarc-checker
export const dmarcDNS = new cloudflare.Record('dmarc', {
  name: '_dmarc',
  type: 'TXT',
  value: 'v=DMARC1; p=reject; rua=mailto:dmarc-reports@upchieve.org; pct=100; adkim=r; aspf=r;',
  zoneId
})

export const mailReverseDNS = new cloudflare.Record('mail-reverse-lookup', {
  name: 'o1.ptr3102',
  type: 'A',
  value: '168.245.66.40',
  proxied: false,
  zoneId
})

export const gleapDkim = new cloudflare.Record('gleap-dkim', {
  name: '20211110194044pm._domainkey',
  type: 'TXT',
  value: 'k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDOUDZt2JR34zyC/OTgbevPTJns46G8zmTqwQ/Ix08yeJiBLXF3hP03Cfp0Xv7tejoJg5wzx7IyyBDLSFIUZfiSjnQBZJqzlvFUEun7pDi6uc2yR30rguwIT/yqjtK1cazkkw/i9CLOSrbn550KsCnud45VAtfjOmcim1q4nYOUNQIDAQAB',
  proxied: false,
  zoneId
})

export const gleapReturnPath = new cloudflare.Record('gleap-return-path', {
  name: 'gm-bounces',
  type: 'CNAME',
  value: 'pm.mtasv.net',
  proxied: false,
  zoneId
})

// these state that we do not send email from this domain, and so not to accept email from it
function noEmailSpfGen(domain: string, zoneId: string) {
  return new cloudflare.Record(`${domain}-spf`, {
    name: domain,
    type: 'TXT',
    value: 'v=spf -all',
    zoneId
  })
}

export const appSpf = noEmailSpfGen('app', zoneId)

export const cdnSpf = noEmailSpfGen('cdn', zoneId)

export const stagingSpf = noEmailSpfGen('staging', zoneId)

export const demoSpf = noEmailSpfGen('demo', zoneId)

export const argoCdSpf = noEmailSpfGen('argocd', zoneId)

export const dataSpf = noEmailSpfGen('data', zoneId)

export const wwwSpf = noEmailSpfGen('www', zoneId)
