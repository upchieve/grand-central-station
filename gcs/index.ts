import * as config from './config'
import * as azure from './azure'
import * as cloudflare from './cloudflare'
import * as azureEast from './azure/east-2'
import * as aiven from './aiven'

config.config

aiven.productionProject
export const aivenProductionProjectId = aiven.productionProject.id
export const aivenStagingProjectId = aiven.stagingProject.id
export const aivenHackersProjectId = aiven.hackersProject.id
export const aivenDemoProjectId = aiven.demoProject.id
export const defaultAivenCloud = aiven.defaultCloud

azure.gcsResourceGroup
azure.gcsNetwork
azure.azureAdmins
export const azureAdminGroupId = azure.azureAdmins.id
azure.cdnDNS
azure.cdnStorageAccount
azure.zwibblerContainer
azure.docsContainer
azure.reviewMaterialsContainer
azure.mobileVersionDocContainer
azure.siteImagesContainer
azure.trainingCoursesContainer
azure.whiteboardDocsStagingStorageAccount
azure.whiteboardDocsStagingContainer
azure.whiteboardDocsProductionStorageAccount
azure.whiteboardDocsProductionContainer
azure.voiceMessagesStagingStorageAccount
azure.voiceMessagesStagingContainer
azure.voiceMessagesProductionStorageAccount
azure.voiceMessagesProductionContainer
azure.assignmentsContainer
azure.assignmentsProductionStorageAccount
azure.assignmentsStagingContainer
azure.assignmentsStagingStorageAccount
azure.subwayStagingApplication
azure.subwayStagingServicePrincipal

azureEast.primaryProvider
export const primaryProviderSubscriptionId = azureEast.primarySubscriptionId
azureEast.gcsResourceGroup
export const gcsResourceGroupLocation = azureEast.gcsResourceGroup.location
export const gcsResourceGroupName = azureEast.gcsResourceGroup.name
azureEast.gcsNetwork
export const gcsNetworkK8sSubnetId = azureEast.gcsNetwork.subnets[0].id
export const gcsNetworkGitlabSubnetId = azureEast.gcsNetwork.subnets[1].id

cloudflare.dmarcDNS
cloudflare.mailReverseDNS
cloudflare.appSpf
cloudflare.cdnSpf
cloudflare.demoSpf
cloudflare.argoCdSpf
cloudflare.dataSpf
cloudflare.stagingSpf
cloudflare.wwwSpf
cloudflare.gleapDkim
cloudflare.gleapReturnPath

