import * as pulumi from "@pulumi/pulumi"

export const removeHelmHooksTransformation = (
  obj: any, opts: pulumi.CustomResourceOptions
) => {
  if (obj.metadata?.annotations?.["helm.sh/hook"]) {
    const {
      "helm.sh/hook": junk,
      ...validAnnotations
    } = obj.metadata.annotations
    obj.metadata.annotations = validAnnotations
  }
}
