import * as aiven from '@pulumi/aiven'
import * as pulumi from '@pulumi/pulumi'
import { config } from '../config'

export const defaultCloud = 'azure-eastus2'

const accountId = 'a31ef95770a1'

export const productionProject = new aiven.Project('upchieve-production', {
  project: 'upchieve-production',
  accountId,
  defaultCloud
})

export const productionPrometheusEndpoint = new aiven.ServiceIntegrationEndpoint('production-newrelic-prometheus-endpoint', {
  endpointType: 'prometheus',
  project: productionProject.id,
  endpointName: 'newrelic',
  prometheusUserConfig: {
    basicAuthUsername: 'newrelic',
    basicAuthPassword: config.requireSecret('newrelicPrometheusProductionPassword')
  }
})

export const productionSyslog = new aiven.ServiceIntegrationEndpoint('production-newrelic-syslog-endpoint', {
  endpointType: 'rsyslog',
  project: productionProject.id,
  endpointName: 'newrelic-logs',
  rsyslogUserConfig: {
    format: 'custom',
    logline: pulumi.interpolate `${config.requireSecret('newRelicKey')} <%pri%>%protocol-version% %timestamp:::date-rfc3339% %hostname% %app-name% %procid% %msgid% -  %msg%`,
    server: 'newrelic.syslog.nr-data.net',
    port: '6514',
    tls: 'true'
  }
})

export const stagingProject = new aiven.Project('upchieve-staging', {
  project: 'upchieve-staging',
  accountId,
  defaultCloud
})

export const stagingPrometheusEndpoint = new aiven.ServiceIntegrationEndpoint('staging-newrelic-prometheus-endpoint', {
  endpointType: 'prometheus',
  project: stagingProject.id,
  endpointName: 'newrelic',
  prometheusUserConfig: {
    basicAuthUsername: 'newrelic',
    basicAuthPassword: config.requireSecret('newrelicPrometheusStagingPassword')
  }
})

export const hackersProject = new aiven.Project('upchieve-hackers', {
  project: 'upchieve-hackers',
  accountId,
  defaultCloud
})

export const hackersPrometheusEndpoint = new aiven.ServiceIntegrationEndpoint('hackers-newrelic-prometheus-endpoint', {
  endpointType: 'prometheus',
  project: hackersProject.id,
  endpointName: 'newrelic',
  prometheusUserConfig: {
    basicAuthUsername: 'newrelic',
    basicAuthPassword: config.requireSecret('newrelicPrometheusHackersPassword')
  }
})

export const demoProject = new aiven.Project('upchieve-demo', {
  project: 'upchieve-demo',
  accountId,
  defaultCloud
})

export const demoPrometheusEndpoint = new aiven.ServiceIntegrationEndpoint('demo-newrelic-prometheus-endpoint', {
  endpointType: 'prometheus',
  project: demoProject.id,
  endpointName: 'newrelic',
  prometheusUserConfig: {
    basicAuthUsername: 'newrelic',
    basicAuthPassword: config.requireSecret('newrelicPrometheusDemoPassword')
  }
})
