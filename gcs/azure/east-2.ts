import {
  core,
  network,
} from '@pulumi/azure'
import * as azure from '@pulumi/azure'

export const primarySubscriptionId = '0c13b281-e8f9-4061-912f-6e27c8ae9073'

export const primaryProvider = new azure.Provider('primary', {
  subscriptionId: primarySubscriptionId
})

export const gcsResourceGroup = new core.ResourceGroup('grand-central-station-east-', {
  location: 'East US 2'
}, { provider: primaryProvider })

export const gcsNetwork = new network.VirtualNetwork('grand-central-station-', {
  location: gcsResourceGroup.location,
  resourceGroupName: gcsResourceGroup.name,
  addressSpaces: ['10.16.0.0/16'],
  subnets: [
    {
      name: 'gcs-k8s',
      addressPrefix: '10.16.0.0/18',
      securityGroup: ''
    }
  ],
}, { provider: primaryProvider })
