import {
  core,
  network,
  storage,
} from '@pulumi/azure'
import {
  Application,
  Group,
  ServicePrincipal
} from '@pulumi/azuread'
import { Assignment } from '@pulumi/azure/authorization'
import * as cloudflare from '@pulumi/cloudflare'
import { config } from '../config'

export const gcsResourceGroup = new core.ResourceGroup('grandCentralStation', {
  location: 'Central US'
})

export const gcsNetwork = new network.VirtualNetwork('grand-central-station', {
  location: gcsResourceGroup.location,
  resourceGroupName: gcsResourceGroup.name,
  addressSpaces: ['10.16.0.0/16'],
  subnets: [
    {
      name: 'gcs-k8s',
      addressPrefix: '10.16.0.0/18',
      securityGroup: ''
    },
  ],
})

export const azureAdmins = new Group('azure-admins', {
  name: 'Azure Admin',
  description: 'Azure Admin'
}, { import: '1b535f14-a6d5-466a-9e97-08fd13925105' })

export const cdnDNS = new cloudflare.Record('cdn', {
  name: 'cdn',
  zoneId: config.get('upchieveZoneId') || 'bogus',
  type: 'CNAME',
  value: 'upchievecdn.blob.core.windows.net',
  ttl: 3600,
})

// equivalent to an S3 bucket in AWS
export const cdnStorageAccount = new storage.Account('cdn-storage', {
  name: 'upchievecdn',
  resourceGroupName: gcsResourceGroup.name,
  location: gcsResourceGroup.location,
  accountTier: 'Standard',
  accountReplicationType: 'GRS',
  accountKind: 'StorageV2',
  customDomain: {
    name: cdnDNS.hostname
  },
  allowBlobPublicAccess: true
})

export const zwibblerContainer = new storage.Container('zwibbler', {
  name: 'zwibbler',
  storageAccountName: cdnStorageAccount.name,
  containerAccessType: 'container',
})

export const docsContainer = new storage.Container('docs', {
  name: 'docs',
  storageAccountName: cdnStorageAccount.name,
  containerAccessType: 'container',
})

export const reviewMaterialsContainer = new storage.Container('review-materials', {
  name: 'review-materials',
  storageAccountName: cdnStorageAccount.name,
  containerAccessType: 'container'
}, { import: 'https://upchievecdn.blob.core.windows.net/review-materials' })

export const mobileVersionDocContainer = new storage.Container('mobile-version-doc', {
  name: 'mobile-version',
  storageAccountName: cdnStorageAccount.name,
  containerAccessType: 'container'
})

// this container should only contain static image assets that service the frontend
export const siteImagesContainer = new storage.Container('site-images', {
  name: 'site-images',
  storageAccountName: cdnStorageAccount.name,
  containerAccessType: 'container'
})

export const trainingCoursesContainer = new storage.Container('training-courses', {
  name: 'training-courses',
  storageAccountName: cdnStorageAccount.name,
  containerAccessType: 'container'
})

/**FrontDoor cdn created on Azure UI */
export const assignmentsStagingStorageAccount = new storage.Account('assignments-staging', {
    name: 'assignmentsstaging',
    resourceGroupName: gcsResourceGroup.name,
    location: gcsResourceGroup.location,
    accountTier: 'Standard',
    accountReplicationType: 'GRS',
    accountKind: 'StorageV2',
    accessTier: 'Cool',
    allowBlobPublicAccess: true,
  })

export const assignmentsProductionStorageAccount = new storage.Account('assignments-production', {
    name: 'assignmentsproduction',
    resourceGroupName: gcsResourceGroup.name,
    location: gcsResourceGroup.location,
    accountTier: 'Standard',
    accountReplicationType: 'GRS',
    accountKind: 'StorageV2',
    accessTier: 'Cool',
    allowBlobPublicAccess: true,
  })

export const assignmentsContainer = new storage.Container('production-assignments', {
  name: 'assignments',
  storageAccountName: assignmentsProductionStorageAccount.name,
  containerAccessType: 'container',
})

export const assignmentsStagingContainer = new storage.Container('staging-assignments', {
  name: 'assignments',
  storageAccountName: assignmentsStagingStorageAccount.name,
  containerAccessType: 'container'
})

export const whiteboardDocsStagingStorageAccount = new storage.Account('whiteboard-docs-staging', {
  name: 'whiteboarddocsstaging',
  resourceGroupName: gcsResourceGroup.name,
  location: gcsResourceGroup.location,
  accountTier: 'Standard',
  accountReplicationType: 'GRS',
  accountKind: 'StorageV2',
  accessTier: 'Cool',
  allowBlobPublicAccess: false
}, { dependsOn: [ cdnDNS ] })

export const whiteboardDocsStagingContainer = new storage.Container('staging-docs', {
  name: 'docs',
  storageAccountName: whiteboardDocsStagingStorageAccount.name,
  containerAccessType: 'private',
})

export const whiteboardDocsProductionStorageAccount = new storage.Account('whiteboard-docs-production', {
  name: 'whiteboarddocsproduction',
  resourceGroupName: gcsResourceGroup.name,
  location: gcsResourceGroup.location,
  accountTier: 'Standard',
  accountReplicationType: 'GRS',
  accountKind: 'StorageV2',
  accessTier: 'Cool',
  allowBlobPublicAccess: false
}, { dependsOn: [ cdnDNS ] })

export const whiteboardDocsProductionContainer = new storage.Container('production-docs', {
  name: 'docs',
  storageAccountName: whiteboardDocsProductionStorageAccount.name,
  containerAccessType: 'private',
})

export const voiceMessagesStagingStorageAccount = new storage.Account('voice-messages-staging', {
  name: 'voicemessagesstaging',
  resourceGroupName: gcsResourceGroup.name,
  location: gcsResourceGroup.location,
  accountTier: 'Standard',
  accountReplicationType: 'GRS',
  accountKind: 'StorageV2',
  accessTier: 'Cool',
  allowBlobPublicAccess: false
}, { dependsOn: [ cdnDNS ] })

export const voiceMessagesStagingContainer = new storage.Container('staging-voice-messgages', {
  name: 'voicemessages',
  storageAccountName: voiceMessagesStagingStorageAccount.name,
  containerAccessType: 'private',
})

export const voiceMessagesProductionStorageAccount = new storage.Account('voice-messages-production', {
  name: 'voicemessagesproduction',
  resourceGroupName: gcsResourceGroup.name,
  location: gcsResourceGroup.location,
  accountTier: 'Standard',
  accountReplicationType: 'GRS',
  accountKind: 'StorageV2',
  accessTier: 'Cool',
  allowBlobPublicAccess: false
}, { dependsOn: [ cdnDNS ] })

export const voiceMessagesProductionContainer = new storage.Container('production-voice-messages', {
  name: 'voicemessages',
  storageAccountName: voiceMessagesProductionStorageAccount.name,
  containerAccessType: 'private',
})

// applications/principals/assignments are necessary for the app to authenticate to
//other resources like the storage accounts
export const subwayStagingApplication = new Application('subway-staging', {
  displayName: 'subway-staging'
})

export const subwayStagingServicePrincipal = new ServicePrincipal('subway-staging', {
  applicationId: subwayStagingApplication.applicationId
})

export const subwayStagingContainerAssignment = new Assignment('subway-staging-containers', {
  scope: whiteboardDocsStagingContainer.resourceManagerId,
  roleDefinitionName: 'Storage Blob Data Contributor',
  principalId: subwayStagingServicePrincipal.id
})

export const subwayStagingContainerAssignmentVoiceMessages = new Assignment('subway-staging-containers-voice-messages', {
  scope: voiceMessagesStagingContainer.resourceManagerId,
  roleDefinitionName: 'Storage Blob Data Contributor',
  principalId: subwayStagingServicePrincipal.id
})

export const subwayProductionApplication = new Application('subway-production', {
  displayName: 'subway-production'
})

export const subwayProductionServicePrincipal = new ServicePrincipal('subway-production', {
  applicationId: subwayProductionApplication.applicationId
})

export const subwayProductionContainerAssignment = new Assignment('subway-production-containers', {
  scope: whiteboardDocsProductionContainer.resourceManagerId,
  roleDefinitionName: 'Storage Blob Data Contributor',
  principalId: subwayProductionServicePrincipal.id
})

export const subwayProductionContainerAssignmentVoiceMessages = new Assignment('subway-production-containers-voice-messages', {
  scope: voiceMessagesProductionContainer.resourceManagerId,
  roleDefinitionName: 'Storage Blob Data Contributor',
  principalId: subwayProductionServicePrincipal.id
})

export const subwayStagingContainerAssignmentAssignments = new Assignment('subway-staging-containers-assignments', {
  scope: assignmentsStagingContainer.resourceManagerId,
  roleDefinitionName: 'Storage Blob Data Contributor',
  principalId: subwayStagingServicePrincipal.id,
})

export const subwayProductionContainerAssignmentTeacherAssignments = new Assignment('subway-production-containers-assignments', {
  scope: assignmentsContainer.resourceManagerId,
  roleDefinitionName: 'Storage Blob Data Contributor',
  principalId: subwayProductionServicePrincipal.id,
})