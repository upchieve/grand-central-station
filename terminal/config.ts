import { Config } from '@pulumi/pulumi'

export const config = new Config()
config.require('ambassadorCloudConnectToken')
config.require('ambassadorWebhookSecret')
config.require('upchieveZoneId')
config.requireSecret('newRelicKey')
