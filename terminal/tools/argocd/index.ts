import * as k8s from '@pulumi/kubernetes'
import * as cloudflare from '@pulumi/cloudflare'
import { terminalProvider } from '../../cluster'
import * as ambassadorCRDs from '../../../crds/ambassador'
import * as certmanagerCRDs from '../../../crds/cert-manager'
import * as ambassador from '../ambassador'
import { config } from '../../config'
import * as helm from '@pulumi/kubernetes/helm'

export const namespace = new k8s.core.v1.Namespace('argocd', {
  metadata: {
    name: 'argocd',
  },
}, { provider: terminalProvider })

export const notificationsConfigMap = new k8s.yaml.ConfigFile('argocd-notifiations-configmap', {
  file: './tools/argocd/notifications-cm.yaml',
}, {
  provider: terminalProvider,
  dependsOn: namespace,
})

// The argocd chart cannot upgrade its own crds
// instructions for upgrading crds are here: https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd#upgrading
export const chart = new helm.v3.Chart('argo-cd', {
  chart: 'argo-cd',
  version: '5.52.2',
  namespace: namespace.metadata.name,
  fetchOpts: {
    repo: 'https://argoproj.github.io/argo-helm',
  },
  values: {
    controller: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }]
    },
    dex: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }]
    },
    redis: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }]
    },
    server: {
      extraArgs: ['--insecure'],
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }]
    },
    repoServer: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }]
    },
    config: {
      accounts: {
        datawire: 'apiKey, login',
        contributors: 'login'
      }
    },
    configs: {
      rbacConfig: {
        policy: {
          default: 'role:readonly',
          csv: `
            p, role:contributor, applications, get, */*, allow
            p, role:contributor, applications, sync, subway/subway-staging, allow
            p, role:contributor, cluster, get, *, allow
            p, role:contributor, repositories, get, *, allow
            p, role:contributor, projects, get, *, allow
            g, contributors, role:contributor,
            p, role:datawire, applications, get, */*, allow
            p, role:datawire, applications, sync, subway/subway-dcp, allow
            p, role:datawire, cluster, get, *, allow
            p, role:datawire, repositories, get, *, allow
            p, role:datawire, projects, get, *, allow
            g, datawire, role:datawire
          `
        },
        secret: {
          argoServerAdminPassword: config.getSecret('argoServerAdminPassword')
        }
      }
    },
    applicationSet: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }]
    },
    notifications: {
      metrics: {
        enabled: true,
        port: 9001
      },
      podAnnotations: {
        'prometheus.io/scrape': 'true',
        'prometheus.io/port': '9001'
      },
      cm: {
        create: false
      },
      secret: {
        items: {
          'slack-token': config.requireSecret('argocdNotificationsSlackToken')
        }
      },
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }]
    }
  },
}, {
  provider: terminalProvider,
  dependsOn: [namespace, notificationsConfigMap]
})

export const argoConsoleMapping = new ambassadorCRDs.getambassador.v3alpha1.Mapping('argo-console', {
  metadata: {
    name: 'argocd-server',
    namespace: namespace.metadata.name,
    labels: {
      host: 'argocd.upchieve.org'
    }
  },
  spec: {
    hostname: 'argocd.upchieve.org',
    prefix: '/',
    service: 'http://argo-cd-argocd-server.argocd',
    timeout_ms: 10000
  },
}, { provider: terminalProvider, dependsOn: ambassador.chart })

export const argoCliMapping = new ambassadorCRDs.getambassador.v3alpha1.Mapping('argo-cli', {
  metadata: {
    name: 'argocd-server-cli',
    namespace: namespace.metadata.name,
    labels: {
      host: 'argocd.upchieve.org'
    }
  },
  spec: {
    hostname: 'argocd.upchieve.org:443',
    prefix: '/',
    service: 'http://argo-cd-argocd-server.argocd',
  },
}, { provider: terminalProvider, dependsOn: ambassador.chart })

const argoDNS = new cloudflare.Record('argocd', {
  name: 'argocd',
  zoneId: config.get('upchieveZoneId') || 'bogus',
  type: 'A',
  value: ambassador.publicIp,
  ttl: 1,
  proxied: true,
})

export const argoCdCert = new certmanagerCRDs.certmanager.v1.Certificate('argocd-cert-self-signed', {
  metadata: {
    name: 'argocd-self-signed',
    namespace: namespace.metadata.name,
  },
  spec: {
    secretName: 'argocd-upchieve-org-self-signed',
    duration: '2160h',
    renewBefore: '360h',
    dnsNames: ['argocd.upchieve.org'],
    issuerRef: {
      name: 'self-signed',
      kind: 'ClusterIssuer',
    }
  }
}, { provider: terminalProvider })

export const argoHost = new ambassadorCRDs.getambassador.v3alpha1.Host('argocd-host', {
  metadata: {
    name: 'argocd',
    namespace: namespace.metadata.name,
  },
  spec: {
    hostname: 'argocd.upchieve.org',
    tlsSecret: {
      name: 'argocd-upchieve-org-self-signed'
    },
    acmeProvider: {
      authority: 'none'
    },
    mappingSelector: {
      matchLabels: {
        host: 'argocd.upchieve.org'
      }
    }
  }
}, { provider: terminalProvider, dependsOn: [argoDNS, argoCdCert] })
