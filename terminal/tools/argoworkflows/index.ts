import * as k8s from '@pulumi/kubernetes'
import { terminalProvider } from '../../cluster'
import * as ambassadorCRDs from '../../../crds/ambassador'
import * as ambassador from '../ambassador'
import * as cloudflare from '@pulumi/cloudflare'
import { config } from '../../config'

export const namespace = new k8s.core.v1.Namespace('argo', {
  metadata: {
    name: 'argo',
  },
}, { provider: terminalProvider })

// export const yamls = new k8s.yaml.ConfigFile('argo', {
//   file: './tools/argoworkflows/workflows.yaml',
// }, {
//   provider: terminalProvider,
//   dependsOn: namespace,
// })

// export const mapping = new ambassadorCRDs.getambassador.v2.Mapping('workflows-console', {
//   metadata: {
//     name: 'argo-workflows-server',
//     namespace: namespace.metadata.name,
//   },
//   spec: {
//     host: 'workflows.upchieve.org',
//     prefix: '/',
//     service: 'http://argo-server.argo:2746',
//   },
// }, { provider: terminalProvider, dependsOn: ambassador.chart })
//
// export const dns = new cloudflare.Record('argo-workflows', {
//   name: 'workflows',
//   zoneId: config.get('upchieveZoneId') || 'bogus',
//   type: 'A',
//   value: ambassador.publicIp,
//   ttl: 1,
//   proxied: true,
// })
//
// export const host = new ambassadorCRDs.getambassador.v2.Host('argo-workflows', {
//   metadata: {
//     name: 'argo-workflows',
//     namespace: namespace.metadata.name,
//   },
//   spec: {
//     hostname: 'workflows.upchieve.org',
//     acmeProvider: {
//       email: 'tech@upchieve.org',
//     },
//   },
// }, { provider: terminalProvider, dependsOn: dns })
