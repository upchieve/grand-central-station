import * as k8s from '@pulumi/kubernetes'
import * as helm from '@pulumi/kubernetes/helm'
import * as pulumi from '@pulumi/pulumi'
import { terminalProvider } from '../../cluster'
import { config } from '../../config'

export const namespace = new k8s.core.v1.Namespace('newrelic', {
  metadata: {
    name: 'newrelic'
  }
}, { provider: terminalProvider })

export const chart = new helm.v3.Chart('newrelic-bundle', {
  chart: 'nri-bundle',
  version: '5.0.25',
  namespace: namespace.metadata.name,
  fetchOpts: {
    repo: 'https://helm-charts.newrelic.com',
  },
  values: {
    global: {
      licenseKey: config.getSecret('newRelicKey'),
      cluster: 'terminal',
      lowDataMode: true
    },
    'newrelic-infrastructure': {
      privileged: true,
      enableProcessMetrics: false,
    },
    'newrelic-logging': {
      enabled: true,
    },
    'nri-prometheus': {
      enabled: true,
      config: {
        insecure_skip_verify: true,
        targets: [
          {
            description: 'staging database prometheus',
            urls: [
              pulumi.interpolate `https://newrelic:${config.requireSecret('newrelicPrometheusStagingPassword')}@subway-pg-upchieve-staging.aivencloud.com:9273/metrics`,
              pulumi.interpolate `https://newrelic:${config.requireSecret('newrelicPrometheusProductionPassword')}@subway-pg-upchieve-production.aivencloud.com:9273/metrics`
            ]
          }
        ]
      }
    },
    'kube-state-metrics': {
      enabled: true,
    },
    'nri-kube-events': {
      enabled: true,
    },
  }
}, { provider: terminalProvider })