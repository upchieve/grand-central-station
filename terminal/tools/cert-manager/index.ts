import * as k8s from '@pulumi/kubernetes'
import * as helm from '@pulumi/kubernetes/helm'
import * as certmanager from '../../../crds/cert-manager'
import { config } from '../../config'
import { terminalProvider } from '../../cluster'

export const namespace = new k8s.core.v1.Namespace('cert-manager', {
  metadata: {
    name: 'cert-manager',
  },
}, { provider: terminalProvider })

// we use cert-manager to handle certificates for linkerd that rotate
// every ~23 hours for high hackers in our mTLS system
export const chart = new helm.v3.Chart('cert-manager', {
  chart: 'cert-manager',
  namespace: namespace.metadata.name,
  fetchOpts: {
    repo: 'https://charts.jetstack.io',
  },
  version: 'v1.10.0',
  values: {
    installCRDs: true,
    tolerations: [{
      key: 'upchieve.org/purpose',
      operator: 'Equal',
      value: 'apps',
      effect: 'NoSchedule',
    }],
    nodeSelector: {
      'upchieve.org/purpose': 'apps',
    },
    webhook: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }],
    },
    cainjector: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }],
    },
  },
}, { provider: terminalProvider })

export const cloudflareSecret = new k8s.core.v1.Secret('cert-manager-cloudflare-secret', {
  metadata: {
    name: 'cloudflare-api-key',
    namespace: namespace.metadata.name
  },
  stringData: {
    apiKey: config.requireSecret('cloudflareApiToken')
  }
}, { provider: terminalProvider })

export const stagingClusterIssuer = new certmanager.certmanager.v1.ClusterIssuer('staging-cluster-issuer', {
  metadata: {
    name: 'letsencrypt-staging',
    namespace: namespace.metadata.name
  },
  spec: {
    acme: {
      email: 'tech@upchieve.org',
      server: 'https://acme-staging-v02.api.letsencrypt.org/directory',
      privateKeySecretRef: {
        name: 'staging-issuer-account-key'
      },
      solvers: [
        {
          dns01: {
            cloudflare: {
              apiTokenSecretRef: {
                name: cloudflareSecret.metadata.name,
                key: 'apiKey'
              }
            }
          }
        }
      ]
    },
  }
})

// this has to wait for our rate limits with Let's Encrypt to reset
// export const productionClusterIssuer = new certmanager.certmanager.v1.ClusterIssuer('production-cluster-issuer', {
//   metadata: {
//     name: 'letsencrypt-production',
//     namespace: namespace.metadata.name
//   },
//   spec: {
//     acme: {
//       email: 'tech@upchieve.org',
//       server: 'https://acme-v02.api.letsencrypt.org/directory',
//       privateKeySecretRef: {
//         name: 'production-issuer-account-key'
//       },
//       solvers: [
//         {
//           dns01: {
//             cloudflare: {
//               apiTokenSecretRef: {
//                 name: cloudflareSecret.metadata.name,
//                 key: 'apiKey'
//               }
//             }
//           }
//         }
//       ]
//     }
//   }
// })

export const selfSignedIssuer = new certmanager.certmanager.v1.ClusterIssuer('self-signed-issuer', {
  metadata: {
    name: 'self-signed',
    namespace: namespace.metadata.name
  },
  spec: {
    selfSigned: {}
  }
})
