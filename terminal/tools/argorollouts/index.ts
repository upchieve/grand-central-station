import * as k8s from '@pulumi/kubernetes'
import { terminalProvider } from '../../cluster'
import { config } from '../../config'
import * as helm from '@pulumi/kubernetes/helm'

const namespace = new k8s.core.v1.Namespace('argo-rollouts', {
  metadata: {
    name: 'argo-rollouts',
  },
}, { provider: terminalProvider })

export const argoRolloutsWebhookSecret = new k8s.core.v1.Secret('argo-rollouts-webhook', {
  metadata: {
    name: 'webhook',
    namespace: namespace.metadata.name,
  },
  data: {
    AMBASSADOR_WEBHOOK_SECRET: config.getSecret('ambassadorWebhookSecret') || 'Ym9ndXMK',
  },
}, { provider: terminalProvider })

export const chart = new helm.v3.Chart('argo-rollouts', {
  chart: 'argo-rollouts',
  version: '2.17.0',
  namespace: namespace.metadata.name,
  fetchOpts: {
    repo: 'https://argoproj.github.io/argo-helm',
  },
  values: {
    installCRDs: true
  },
}, { provider: terminalProvider })
