import * as k8s from '@pulumi/kubernetes'
import * as helm from '@pulumi/kubernetes/helm'
import * as ambassadorCRDs from '../../../crds/ambassador/index'
import * as certmanagerCRDs from '../../../crds/cert-manager'
import { terminalProvider } from '../../cluster'
import { config } from '../../config'

const namespace = new k8s.core.v1.Namespace('ambassador', {
  metadata: {
    name: 'ambassador',
  },
}, { provider: terminalProvider })

// The edge-stack chart does not install its own crds so we are installing them ourselves
export const edgeStackCRDs = new k8s.yaml.ConfigFile('edge-stack-3-crds', {
  file: './tools/ambassador/aes-crds.yaml',
}, {
  provider: terminalProvider,
  dependsOn: namespace,
})

export const chart = new helm.v3.Chart('ambassador', {
  chart: 'edge-stack',
  version: '8.9.3',
  namespace: namespace.metadata.name,
  fetchOpts: {
    repo: 'https://s3.amazonaws.com/datawire-static-files/charts',
  },
  values: {
    authService: {
      create: false,
    },
    rateLimit: {
      create: false,
    },
    'emissary-ingress': {
      // gives us some nice stuff at https://app.getambassador.io
      agent: {
        cloudConnectToken: config.getSecret('ambassadorCloudConnectToken'),
      },
      createDefaultListeners: false,
      daemonSet: true,
      podAnnotations: {
        'prometheus.io/scrape': 'true',
        'prometheus.io/port': '8877'
      },
      module: {
        defaults: {
          httpmapping: {
            timeout_ms: 500000,
          },
        }
      },
      resources: {
        limits: {
          cpu: '300m',
          memory: '500Mi',
        },
        requests: {
          cpu: '100m',
          memory: '300Mi',
        },
      },
      podDisruptionBudget: {
        minAvailable: 1,
      },
      service: {
        annotations: {
          'a8r.io/description': 'Entry point for the cluster, handles all ingress and routes to other services.',
          'a8r.io/owner': 'dave.sudia@upchieve.org',
          'a8r.io/chat': 'http://a8r.io/Slack',
          'a8r.io/bugs': 'https://github.com/datawire/ambassador/issues',
          'a8r.io/logs': 'https://one.newrelic.com/launcher/logger.log-launcher',
          'a8r.io/documentation': 'https://www.getambassador.io/docs/edge-stack/latest/',
          'a8r.io/repository': 'https://github.com/datawire/ambassador',
          'a8r.io/support': 'https://www.getambassador.io/about-us/support/',
          'a8r.io/incidents': 'https://app.allma.io',
          'a8r.io/uptime': 'https://upchieve.statuspage.io',
          'a8r.io/performance': 'https://onenr.io/0Z2R57JE1Qb',
          'a8r.io/dependencies': 'ambassador-redis.ambassador'
        }
      },
      // these tolerations/selectors make sure this lands on the app pool
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'apps',
        effect: 'NoSchedule',
      }],
      nodeSelector: {
        'upchieve.org/purpose': 'apps',
      },
    },
    licenseKey: {
      value: config.requireSecret('edgeStackLicenseKey'),
    },
    createDevPortalMappings: false,
    redis: {
      tolerations: [{
        key: 'upchieve.org/purpose',
        operator: 'Equal',
        value: 'infra',
        effect: 'PreferNoSchedule',
      }],
    },
  }
}, { provider: terminalProvider, dependsOn: edgeStackCRDs })

export const httpListener = new ambassadorCRDs.getambassador.v3alpha1.Listener('http-listener', {
  metadata: {
    name: 'http-listener',
    namespace: namespace.metadata.name
  },
  spec: {
    port: 8080,
    protocol: 'HTTP',
    securityModel: 'XFP',
    hostBinding: {
      namespace: {
        from: 'ALL'
      }
    }
  }
}, { provider: terminalProvider })

export const httpsListener = new ambassadorCRDs.getambassador.v3alpha1.Listener('https-listener', {
  metadata: {
    name: 'https-listener',
    namespace: namespace.metadata.name
  },
  spec: {
    port: 8443,
    protocol: 'HTTPS',
    securityModel: 'XFP',
    hostBinding: {
      namespace: {
        from: 'ALL'
      }
    }
  }
}, { provider: terminalProvider })

// const ambassadorSvc = chart.getResource('v1/Service', 'ambassador/ambassador')

// allows us to target this IP address in Cloudflare elsewhere
export const publicIp = '20.7.58.157'

export const wildcardCert = new certmanagerCRDs.certmanager.v1.Certificate('wildcard-cert-self-signed', {
  metadata: {
    name: 'wildcard-self-signed',
    namespace: namespace.metadata.name,
  },
  spec: {
    secretName: 'wildcard-upchieve-org-self-signed',
    duration: '2160h',
    renewBefore: '360h',
    dnsNames: ['*.upchieve.org'],
    issuerRef: {
      name: 'self-signed',
      kind: 'ClusterIssuer',
    }
  }
}, { provider: terminalProvider })

export const wildcardUpchieveHost = new ambassadorCRDs.getambassador.v3alpha1.Host('wildcard-upchieve', {
  metadata: {
    name: 'wildcard-upchieve',
    namespace: namespace.metadata.name,
  },
  spec: {
    hostname: '*.upchieve.org',
    acmeProvider: {
      authority: 'none'
    },
    tlsSecret: {
      name: 'wildcard-upchieve-org-self-signed',
    },
  },
}, { provider: terminalProvider, dependsOn: [chart, wildcardCert] })

// we can use this in mappings to get sticky sessions where needed
export const k8sEndpointResolver = new ambassadorCRDs.getambassador.v3alpha1.KubernetesEndpointResolver('endpoint', {
  metadata: {
    namespace: namespace.metadata.name,
    name: 'endpoint',
  },
}, { provider: terminalProvider, dependsOn: chart })
