import * as cloudflare from '@pulumi/cloudflare'
import { config } from '../../config'


// This setup allows us to proxy our product analytics traffic to Posthog from our domain
// allowing us to avoid tracker blockers by having the traffic to our first party domain
// the DNS is `p.upchieve.org` because Posthog has seen blockers block traffic to any domain with `analytics` in it

const caddyPublicIp = '20.124.154.93'

export const dns = new cloudflare.Record('product-analytics', {
  name: 'p',
  zoneId: config.get('upchieveZoneId') || 'bogus',
  type: 'A',
  value: caddyPublicIp,
  ttl: 1,
  proxied: true,
})
