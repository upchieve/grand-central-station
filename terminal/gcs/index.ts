import { StackReference } from '@pulumi/pulumi'

const gcs = new StackReference('upchieve/grand-central-station/gcs')

export const k8sSubnetId = gcs.requireOutput('gcsNetworkK8sSubnetId')
export const primarySubscriptionId = gcs.requireOutput('primaryProviderSubscriptionId')
export const resourceGroupName = gcs.requireOutput('gcsResourceGroupName')
export const resourceGroupLocation = gcs.requireOutput('gcsResourceGroupLocation')
export const azureAdminGroupId = gcs.requireOutput('azureAdminGroupId')
