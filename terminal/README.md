# Terminal

Primary kubernetes cluster in Azure US East2.

## Installed applications

### Ambassador

An API gateway that controls all traffic into our cluster. It is the only service with a public IP address via a LoadBalancer service. Also handles all public facing TLS.

### LinkerD

Service mesh that provides mTLS inside the cluster as well as observability between services.

### Cert Manager

Handles root certificate authority management for LinkerD.

### ArgoCD

Our CD system, GitOps style, connected with the Port Authority repo.

### NewRelic

Monitoring and logging system.

## Generating TypeScript types from CustomResourceDefinitions (CRDs)

We use [crd2pulumi](https://github.com/pulumi/crd2pulumi) to generate TS types from the CRDs. This only needs to be done
when there are new CRDs for a given application, usually when there's a major version upgrade. An example command is
* `$ crd2pulumi --force --nodejsPath=./crds/ambassador ./terminal/tools/ambassador/aes-crds.yaml`

--force is used because it will overwrite the directory without you having to delete everything first. Once the TS types 
are genererated you can import them to create type-safe object definitions in the rest of the code.