import * as k8s from '@pulumi/kubernetes'
import { terminalProvider } from '../cluster'

export const stagingNamespace = new k8s.core.v1.Namespace('staging', {
  metadata: {
    name: 'staging',
  },
}, { provider: terminalProvider })

export const demoNamespace = new k8s.core.v1.Namespace('demo', {
  metadata: {
    name: 'demo',
  },
}, { provider: terminalProvider })

export const productionNamespace = new k8s.core.v1.Namespace('production', {
  metadata: {
    name: 'production',
  },
}, { provider: terminalProvider })

export const hackersNamespace = new k8s.core.v1.Namespace('hackers', {
  metadata: {
    name: 'hackers',
  },
}, { provider: terminalProvider })
