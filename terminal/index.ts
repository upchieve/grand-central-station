import * as cluster from './cluster'
import * as certmanager from './tools/cert-manager'
import * as ambassador from './tools/ambassador'
import { namespace, chart, argoCliMapping, argoConsoleMapping, argoCdCert, argoHost} from './tools/argocd'
import * as argorollouts from './tools/argorollouts'
import * as app from './app'
import * as newrelic from './tools/newrelic'
import * as workflows from './tools/argoworkflows'
import * as posthog from './tools/posthog'

cluster.terminalCluster
cluster.infraPool
cluster.appPool
cluster.appPoolLarge
export const terminalKubeConfig = cluster.terminalCluster.kubeAdminConfigRaw

certmanager.chart
certmanager.cloudflareSecret
certmanager.stagingClusterIssuer
certmanager.selfSignedIssuer

ambassador.edgeStackCRDs
ambassador.chart
ambassador.k8sEndpointResolver
ambassador.httpListener
ambassador.httpsListener
export const ambassadorIp = ambassador.publicIp
ambassador.wildcardCert
ambassador.wildcardUpchieveHost

chart
argoCliMapping
argoConsoleMapping
argoCdCert
argoHost
export const argocdNamespace = namespace.metadata.name

argorollouts.argoRolloutsWebhookSecret
argorollouts.chart

export const demoNamespace = app.demoNamespace.metadata.name
export const stagingNamespace = app.stagingNamespace.metadata.name
export const productionNamespace = app.productionNamespace.metadata.name
export const hackersNamespace = app.hackersNamespace.metadata.name

newrelic.chart

workflows.namespace

posthog.dns
