import {
  containerservice,
  Provider,
} from '@pulumi/azure'
import * as k8s from '@pulumi/kubernetes'
import * as gcs from '../gcs'

export const primaryProvider = new Provider('primary', {
  subscriptionId: gcs.primarySubscriptionId,
})

export const terminalCluster = new containerservice.KubernetesCluster('terminal', {
  // allows us to enforce hackers policies
  // like pods not being able to use host network/storage
  // and other best practices
  addonProfile: {
    azurePolicy: {
      enabled: true,
    },
  },
  defaultNodePool: {
    // spreads nodes across zones for HA
    availabilityZones: ['1', '2', '3'],
    name: 'default',
    vmSize: 'Standard_B2s',
    vnetSubnetId: gcs.k8sSubnetId,
    nodeCount: 1,
    onlyCriticalAddonsEnabled: true,
    // burstable VMs only support managed disks, which cost more than ephemeral
    osDiskType: 'Managed',
    // this is the smallest disk size supported
    osDiskSizeGb: 32,
    upgradeSettings: {
      maxSurge: '1',
    },
  },
  dnsPrefix: 'upchieve-gcs',
  identity: {
    type: 'SystemAssigned',
  },
  kubernetesVersion: '1.27.3',
  location: gcs.resourceGroupLocation,
  networkProfile: {
    networkPlugin: 'azure',
    networkPolicy: 'azure',
    serviceCidr: '10.16.64.0/20',
    dnsServiceIp: '10.16.64.10',
    dockerBridgeCidr: '172.17.0.1/16',
  },
  resourceGroupName: gcs.resourceGroupName,
  roleBasedAccessControl: {
    enabled: true,
    azureActiveDirectory: {
      managed: true,
      adminGroupObjectIds: [gcs.azureAdminGroupId],
    },
  },
  skuTier: 'Paid',
}, { provider: primaryProvider })

export const infraPool = new containerservice.KubernetesClusterNodePool('terminal-infra-pool', {
  name: 'infra',
  kubernetesClusterId: terminalCluster.id,
  availabilityZones: ['1', '2', '3'],
  vmSize: 'Standard_B2s',
  vnetSubnetId: gcs.k8sSubnetId,
  enableAutoScaling: true,
  minCount: 4,
  maxCount: 6,
  // k8s will try not to put things on this node unless there's no room elsewhere
  // apps we want to land on this node pool should have a taint toleration
  // and use a nodeSelector matching the label
  nodeTaints: ['upchieve.org/purpose=infra:PreferNoSchedule'],
  nodeLabels: {
    'upchieve.org/purpose': 'infra',
  },
  osDiskType: 'Managed',
  osDiskSizeGb: 32,
  upgradeSettings: {
    maxSurge: '1',
  },
}, { provider: primaryProvider })

export const appPool = new containerservice.KubernetesClusterNodePool('terminal-app-pool', {
  name: 'apps',
  kubernetesClusterId: terminalCluster.id,
  availabilityZones: ['1', '2', '3'],
  vmSize: 'Standard_F4s_v2',
  // this is how we avoid paying absurd sums for disk!
  osDiskType: 'Ephemeral',
  osDiskSizeGb: 32,
  vnetSubnetId: gcs.k8sSubnetId,
  enableAutoScaling: false,
  nodeCount: 1,
  // k8s will not schedule pods on this at all unless they tolerate the taint
  nodeTaints: ['upchieve.org/purpose=apps:NoSchedule'],
  // apps that we want to have land on this node pool should have
  // a nodeSelector matching this label
  nodeLabels: {
    'upchieve.org/purpose': 'apps',
  },
  upgradeSettings: {
    maxSurge: '1',
  },
}, { provider: primaryProvider })

export const appPoolLarge = new containerservice.KubernetesClusterNodePool('terminal-app-pool-large', {
  name: 'appslrg',
  kubernetesClusterId: terminalCluster.id,
  availabilityZones: ['1', '2', '3'],
  vmSize: 'Standard_F8s_v2',
  // this is how we avoid paying absurd sums for disk!
  osDiskType: 'Ephemeral',
  osDiskSizeGb: 32,
  vnetSubnetId: gcs.k8sSubnetId,
  enableAutoScaling: false,
  nodeCount: 2,
  // k8s will not schedule pods on this at all unless they tolerate the taint
  nodeTaints: ['upchieve.org/purpose=apps:NoSchedule'],
  // apps that we want to have land on this node pool should have
  // a nodeSelector matching this label
  nodeLabels: {
    'upchieve.org/purpose': 'apps',
  },
  upgradeSettings: {
    maxSurge: '1',
  },
}, { provider: primaryProvider })

export const terminalProvider = new k8s.Provider('terminal', {
  kubeconfig: terminalCluster.kubeAdminConfigRaw,
  suppressDeprecationWarnings: true,
})
