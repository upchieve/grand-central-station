# Marathon

Gitlab runner instances.

Instances get created through Pulumi, but are set up manually.

### Setup instructions

1. Create instance via Pulumi
   * add instance in index.ts
   * run Pulumi up
1. Reset instance password
   * go to DigitalOcean console
   * choose your new instance
   * Go to `Access` tab
   * Choose to reset the root password
   * You will get an email with the new password
1. SSH to the instance
   * Change the root password at the prompt
   * Store it in 1password as a server
1. Install Docker and Gitlab Runner
   * `scp` the userdata.sh script in this directory to the VM
   * change the token value to the value from the CI Settings panel of the Gitlab org
   * change the name (description field)
   * run the script

### Future work

Theoretically we can get the userdata script running via the userData
parameter that can be passed to the Droplet object in Pulumi.
Dave could not figure that out on first attempt here was spinning his wheels,
so if anyone wants to take a stab at it, please do!
