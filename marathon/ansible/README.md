# Running migrations

## Tooling
- We use **Ansible Playbooks** to define our migration steps and initiate them
- The playbooks are instructed to use the **dbmate** library to run our migrations against the database
- We run our Ansible playbooks inside of a Python virtual environment (**virtualenv** package)
- We use a DB jumpbox to execute the playbooks/migrations

## Setup

- Set up python: `brew install python3`
- Install pip: `python3 -m pip install --upgrade pip`
- Install virtualenv: `pip3 install virtualenv`
- In the `ansible` directory:
  - Create the virtual environment: `$ virtualenv venv`
  - Activate/enter the virtual environment: `$ source venv/bin/activate`
  - Install ansible: `$ pip3 install ansible`
- [Install 1Password CLI](https://developer.1password.com/docs/cli/v1/usage/). We need this to pull the SSH key for the DB jumpbox.
- Log into 1Pass CLI
- Enable use of the SSH agent. The easiest way to do this is to: 
  - Add the following to your `~/.ssh/config` file (this is a modification of the steps described on [1Password's website](https://developer.1password.com/docs/ssh/get-started/#step-4-configure-your-ssh-or-git-client)):
  
  ```
  Host 161.35.115.215
    User root
    Hostname 161.35.115.215
    IdentityAgent "~/Library/Group Containers/2BUA8C4S2C.com.1password/t/agent.sock"
  ```  

  - Download the 1Password desktop app and log in
  - Navigate to Settings -> Developer and check off the following:
    - [X] Use the SSH agent
    - [X] Integrate with 1Password-CLI

## Running playbooks

Do:
`$ ansible-playbook -i hosts <playbook.yml> --vault-password-file ./ansible_vault_pass.sh`

Where:
* `playbook.yml` is the playbook to run, i.e. `run_staging_migrations.yml`
* `-i hosts` tells it to look at the `hosts` file for potential places to run.
* `--vault-password-file ./ansible_vault_pass.sh` tells it to use that file to get the decryption password for secrets
  * you must have the 1Password cli installed and activated for this to work

## Structure

The directory is split into playbooks at the top level, and roles below that.
Roles can be used by multiple playbooks, and also work to make things more readable
by splitting tasks into smaller, related pieces we can chain together.

There are `vars` directories at the top level and in some roles. The directory at
the top level is for vars/secrets that are shared across roles. For example, multiple
roles need the production postgres host address. Vars/secrets in a role are specific to
that role, like the NewRelic license key.
