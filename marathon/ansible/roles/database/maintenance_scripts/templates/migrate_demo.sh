#! /usr/bin/env bash

dbmate \
  -d ./migrations \
  --url "postgres://{{ pg_user }}:{{ pg_demo_admin_password }}@{{ pg_demo_host}}:{{ pg_demo_port }}/{{ pg_database }}" \
  up

dbmate \
  -d ./seed-updates \
  --no-dump-schema \
  --migrations-table seed_migrations \
  --url "postgres://{{ pg_user }}:{{ pg_demo_admin_password }}@{{ pg_demo_host}}:{{ pg_demo_port }}/{{ pg_database }}" \
  up
  
psql \
  -d "postgres://{{ pg_user }}:{{ pg_demo_admin_password }}@{{ pg_demo_host}}:{{ pg_demo_port }}/{{ pg_database }}" \
  -f ./db_init/auth.sql
