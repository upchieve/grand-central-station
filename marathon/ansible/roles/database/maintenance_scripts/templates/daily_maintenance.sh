#! /usr/bin/env bash

psql \
  -h {{ pg_production_host }} \
  -p {{ pg_production_port }} \
  -U {{ pg_user }} \
  -f /home/dbmanager/subway/database/maintenance/analyze.sql