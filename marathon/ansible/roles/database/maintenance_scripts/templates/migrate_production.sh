#! /usr/bin/env bash

dbmate \
  -d ./migrations \
  --url "postgres://{{ pg_user }}:{{ pg_production_admin_password }}@{{ pg_production_host}}:{{ pg_production_port }}/{{ pg_database }}" \
  up

dbmate \
  -d ./seed-updates \
  --no-dump-schema \
  --migrations-table seed_migrations \
  --url "postgres://{{ pg_user }}:{{ pg_production_admin_password }}@{{ pg_production_host}}:{{ pg_production_port }}/{{ pg_database }}" \
  up

psql \
  -d "postgres://{{ pg_user }}:{{ pg_production_admin_password }}@{{ pg_production_host}}:{{ pg_production_port }}/{{ pg_database }}" \
  -f ./db_init/auth.sql
