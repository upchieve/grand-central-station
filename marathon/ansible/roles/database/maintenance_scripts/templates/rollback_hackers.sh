#! /usr/bin/env bash

dbmate \
  -d ./migrations \
  --url "postgres://{{ pg_user }}:{{ pg_hackers_admin_password }}@{{ pg_hackers_host}}:{{ pg_hackers_port }}/{{ pg_database }}" \
  rollback

psql \
  -d "postgres://{{ pg_user }}:{{ pg_hackers_admin_password }}@{{ pg_hackers_host}}:{{ pg_hackers_port }}/{{ pg_database }}" \
  -f ./db_init/auth.sql
