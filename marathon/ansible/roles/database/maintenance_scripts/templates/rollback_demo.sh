#! /usr/bin/env bash

dbmate \
  -d ./migrations \
  --url "postgres://{{ pg_user }}:{{ pg_demo_admin_password }}@{{ pg_demo_host}}:{{ pg_demo_port }}/{{ pg_database }}" \
  rollback

psql \
  -d "postgres://{{ pg_user }}:{{ pg_demo_admin_password }}@{{ pg_demo_host}}:{{ pg_demo_port }}/{{ pg_database }}" \
  -f ./db_init/auth.sql
