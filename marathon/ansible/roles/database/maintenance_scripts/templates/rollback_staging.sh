#! /usr/bin/env bash

dbmate \
  -d ./migrations \
  --url "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
  rollback

psql \
  -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
  -f ./db_init/auth.sql
