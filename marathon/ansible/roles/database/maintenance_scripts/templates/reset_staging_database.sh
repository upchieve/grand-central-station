#! /usr/bin/env bash

psql \
 -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
 -c "DROP TABLE IF EXISTS public.schema_migrations;"

psql \
 -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
 -c "DROP TABLE IF EXISTS public.seed_migrations;"

psql \
  -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
  -c "DROP SCHEMA IF EXISTS upchieve CASCADE;"

psql \
 -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
 -c "DROP SCHEMA IF EXISTS auth CASCADE;"

psql \
  -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
  -f ./db_init/schema.sql

psql \
  -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
  -f ./db_init/auth.sql

psql \
  -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
  -f ./db_init/test_seeds.sql

psql \
  -d "postgres://{{ pg_user }}:{{ pg_staging_admin_password }}@{{ pg_staging_host}}:{{ pg_staging_port }}/{{ pg_database }}" \
  -f ./db_init/seed_migrations.sql
