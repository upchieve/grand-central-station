import * as pulumi from '@pulumi/pulumi'
import * as digitalocean from '@pulumi/digitalocean'

const config = new pulumi.Config()

export const provider = new digitalocean.Provider('default-digitalocean-provider', {
  token: config.requireSecret('digitaloceanToken')
})

const dbJumpBoxSshKey = new digitalocean.SshKey('db-jump-box-key', {
  name: 'db-jump-box',
  publicKey: 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKu1N/uZ/bYR48trSTO2LwvglcmBmF/eQz3+sb42slR9'
}, { provider })

const dbJumpBox = new digitalocean.Droplet('db-jump-box', {
  name: 'db-jump-box',
  image: 'ubuntu-20-04-x64',
  region: 'nyc1',
  size: 's-1vcpu-2gb',
  monitoring: true,
  tags: ['database', 'production'],
  sshKeys: [dbJumpBoxSshKey.id],
  backups: true
}, { provider })

export const jumpBoxIp = dbJumpBox.ipv4Address
