#! /usr/bin/env bash

apt update
apt upgrade -y

curl -fsSL https://get.docker.com -o get-docker.sh
bash get-docker.sh

systemctl enable docker
systemctl start docker

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
export GITLAB_RUNNER_DISABLE_SKEL=true; apt install gitlab-runner

systemctl enable gitlab-runner
systemctl start gitlab-runner

gitlab-runner register -n \
  --url https://gitlab.com/ \
  --request-concurrency 2 \
  --registration-token <token> \
  --executor docker \
  --description <name> \
  --docker-image docker:20.10.6 \
  --tag-list "upchieve" \
  --run-untagged="false" \
  --docker-privileged \
  --docker-volumes '/certs/client'
